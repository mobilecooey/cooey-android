package com.biz.health.cooey_app;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.biz.health.model.MedicineData;
import com.lifesense.ble.bean.LsDeviceInfo;

public class MyMedicinesAdapter<T> extends ArrayAdapter<T> {

	
	private List<T> listMeds = null;
	private LayoutInflater inflater = null;
	private int layoutResource;
	private Context ctx;
	private static final int CORNER_RADIUS = 24; // dips
	private static final int MARGIN = 12;
	private float density;
	private Typeface QS_font;
	
	public MyMedicinesAdapter(Context context, int resource, List<T> objects) {
		super(context, resource, objects);
		ctx = context;
		listMeds = objects;
		layoutResource = resource;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		density = ctx.getResources().getDisplayMetrics().density;
		QS_font = Typeface.createFromAsset(ctx.getAssets(),
				"fonts/Quicksand_Book.otf");
		
	}
		
	public int getCount() {
        return listMeds.size();
    }
 
    public T getItem(int position) {
        return listMeds.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
    
    private class DeleteMedicineButtonClickListener implements OnClickListener{

		private MedicineData curMed = null;
		private MyMedicinesAdapter<T> adapter;
		private int pos2del = -1;
		public DeleteMedicineButtonClickListener(MedicineData _med, int position, MyMedicinesAdapter<T> _ctx){
			curMed = _med;
			pos2del = position;
			adapter = _ctx;
		}
		@Override
		public void onClick(View v) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(
					ctx).setTitle("Do you want to delete?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {							
							((Activity) ctx).runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									((MainActivity)ctx).removeMedicine(curMed);	
									Log.d("DEL PPOSITION", String.valueOf(pos2del));
									listMeds.remove(pos2del);
									adapter.notifyDataSetChanged();
								}
							});
							StatsUtil.stat_triedDelMed(ctx);
						}
					}).setNegativeButton("No", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}

					}).setMessage("Medicines can be added again by \"Add Medicine\" from the menu");
			builder.create().show();
		}
	}
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

    	Holder holder =new Holder();
		if(convertView == null)
		{			
			convertView =inflater.inflate(layoutResource, parent, false);
		}
		MedicineData med = (MedicineData)getItem(position);
		
		holder.txtmedname = (TextView) convertView.findViewById(R.id.txtlstmedname);
		holder.txtmedname.setText(med.getMedicineName());
		holder.txtmedname.setTypeface(QS_font);
		
		holder.txtdosage = (TextView) convertView.findViewById(R.id.txtlstdos);
		holder.txtdosage.setText(" Dosage selected : " + med.getDosage());
		holder.txtdosage.setTypeface(QS_font);
		
		holder.txtreminder = (TextView) convertView.findViewById(R.id.txtlstremnd);
		if(med.getReminders().isEmpty()){
			holder.txtreminder.setText(" No reminders set. ");
		}else{
			holder.txtreminder.setText(" You will be reminded on \n"+ med.getReminders());
		}
		holder.txtreminder.setTypeface(QS_font);
		
		holder.btndel = (ImageButton) convertView.findViewById(R.id.btnmedlstdel);
		
		holder.btndel.setOnClickListener(null);
		holder.btndel.setOnClickListener(new DeleteMedicineButtonClickListener((MedicineData)listMeds.get(position), position, this));
		
		/*new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//delete the medicine
				((MainActivity)ctx).removeMedicine((MedicineData)listMeds.get(position));
			}
		});*/
		//convertView.setTag(holder);
		/*}else{
			holder = (Holder) convertView.getTag();
			//holder.btndel.setOnClickListener(null);
			//holder.btndel.setOnClickListener(new DeleteMedicineButtonClickListener((MedicineData)listMeds.get(position), position, this));
		}*/
		
    	return convertView;
    }
	
    class Holder
	{
		public TextView txtmedname = null;
		public TextView txtdosage = null;
		public TextView txtreminder = null;
		public ImageButton btndel = null;
	}
    
}
