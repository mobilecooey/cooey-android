package com.biz.health.cooey_app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyDeviceDataReceiveCallback;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.WeightData;
import com.biz.health.utils.db.CooeyBPDataSource;
import com.biz.health.utils.db.CooeyWTDataSource;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.OnChartGestureListener;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.lifesense.ble.bean.LsDeviceInfo;

public class TakeBPReading extends Fragment implements OnChartGestureListener, OnChartValueSelectedListener {
	
	private CooeyBleDeviceManager mManager = null;
	
	private BPData mLastKnownBp = null;
	
	ArrayList<BarEntry> ySys = new ArrayList<BarEntry>();
    ArrayList<BarEntry> yDia = new ArrayList<BarEntry>();
    ArrayList<BarEntry> yPul = new ArrayList<BarEntry>();
    ArrayList<String> xVals = new ArrayList<String>();
    ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
    private View rootView;
    private View arrRedOrng, arrOrng, arrGrn, arrGrnRed, arrFullRed;
    private GraphicalView mChartView;
    XYSeries SysSeries, DiaSeries, PulseSeries;
    private int maxPermissableX = 30;
    private CooeyProfile activeProfile = null;
    private ListView lvBps;
    private List<BPData> lstbps;
    private int SHOW_MAX_X = 6;
    private ImageView imgAddManual;
    
    private XYSeriesRenderer DiaBarRenderer, SysBarRenderer, pulseLineRenderer;
    private XYMultipleSeriesRenderer multiRenderer;
    private int margins[] = { 0, 50, 50, 1 };
    
	private class sydia{
		private Float sys;
		private Float dia;
		private String date;
		public Float getSys() {
			return sys;
		}
		public void setSys(Float sys) {
			this.sys = sys;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public Float getDia() {
			return dia;
		}
		public void setDia(Float dia) {
			this.dia = dia;
		}
		
	}
	
	private ArrayList<sydia> mydata = new ArrayList<TakeBPReading.sydia>();
	
	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
	}
	@Override
	public void onDestroyView() {
	
		super.onDestroyView();
		mManager.shutdown();
		((MainActivity)getActivity()).changeBluetoothState(false);
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//mManager.shutdown();
	}
	
	
	/*
	
	 * Blood Pressure
		Category 					Systolic 				Diastolic
		Normal 						less than 120 	and 	less than 80
		Prehypertension 			120 � 139 	or 	80 � 89
		High Blood Pressure
		(Hypertension) Stage 1 		140 � 159 	or 	90 � 99
		High Blood Pressure
		(Hypertension) Stage 2 		160 or higher 	or 	100 or higher
		Hypertensive Crisis
		(Emergency care needed) 	Higher than 180 	or 	Higher than 110

	 */
	private int getZoneForBPSys(int sys){

		if(sys < 125){ return 2;}
		if(sys > 125 && sys <= 139) { return 1; }
		if(sys > 139 && sys <= 159) { return 0; }
		if(sys > 159 && sys <= 179) { return 3; }
		//if(sys > 179) { return 4; }
		return 4;
		//return color;	
	}
	
	private int getZoneForBPDia(int dia){
		if(dia < 85){ return 2; }
		if(dia > 85 && dia <= 89) { return 1; }
		if(dia > 89 && dia <= 99) { return 2; }
		if(dia > 99 && dia <= 109) { return 3; }
		//if(dia > 109) { return R.color.Red; }
		return 4;
	}

	
	private String getSQLForToday(){
		return " date(date, 'start of day') > date('now', 'start of day') ";
	}
	
	private String getSQLForLastWeek(){
		return " date(date, 'start of day') >= date('now', '-7 days') ";
	}
	
	private String getSQLForLastMonth(){
		return " date(date, 'start of day') >= date('now', '-30 days') ";//start of month
	}
	
	private void setVisibilityOf(View v, int visibility){
		
		arrRedOrng.setVisibility(View.INVISIBLE);
		arrOrng.setVisibility(View.INVISIBLE);
		arrGrn.setVisibility(View.INVISIBLE);
		arrGrnRed.setVisibility(View.INVISIBLE);
		arrFullRed.setVisibility(View.INVISIBLE);
		v.setVisibility(visibility);
		
	}
	private void setArrowPosition(int s, int d){
		
		int zone_sys = getZoneForBPSys(s);
		int zone_dia = getZoneForBPDia(d);
		if(zone_dia == 2 && zone_sys == 2){
			//arrowgreen
			setVisibilityOf(arrGrn, View.VISIBLE);
		}else{
			if(zone_dia == 1 || zone_sys == 1){
				//green n orange
				//arroworange
				setVisibilityOf(arrOrng, View.VISIBLE);
				return;
			}
			if(zone_dia == 0 || zone_sys == 0){
				//ornage
				//arrowred1
				setVisibilityOf(arrRedOrng, View.VISIBLE);
				return;
			}
			if(zone_dia == 3 || zone_sys == 3){
				//green n red
				//arrowred2
				setVisibilityOf(arrGrnRed, View.VISIBLE);
				return;
			}
			if(zone_dia == 4 || zone_sys == 4){
				//full red
				//arrowred3
				setVisibilityOf(arrFullRed, View.VISIBLE);
				return;
			}
		}
	}

	//////////////////////////////////////////////////////////
	
	
	private void setDiastolicRenderer() {

	    DiaBarRenderer = new XYSeriesRenderer();
	    DiaBarRenderer.setColor(getActivity().getResources().getColor(
	            R.color.greenreading));
	    DiaBarRenderer.setFillPoints(true);
	    DiaBarRenderer.setLineWidth(1);
	    DiaBarRenderer.setChartValuesTextAlign(Align.LEFT);
	    DiaBarRenderer.setChartValuesTextSize(20f);
	    DiaBarRenderer.setDisplayChartValues(true);
	    //DiaBarRenderer.setChartValuesSpacing(10f);   
	    
	}

	private void setSystolicRenderer() {

	    SysBarRenderer = new XYSeriesRenderer();
	    SysBarRenderer.setColor(getActivity().getResources().getColor(
	            R.color.orangereading));	   
	    SysBarRenderer.setFillPoints(true);
	    SysBarRenderer.setLineWidth(1);
	    SysBarRenderer.setChartValuesTextAlign(Align.LEFT);
	    SysBarRenderer.setChartValuesTextSize(20f);
	    SysBarRenderer.setDisplayChartValues(true);
	}

	private void setMultiRenderer() {

	    multiRenderer = new XYMultipleSeriesRenderer();
	    //multiRenderer.setChartTitle("Bar Chart and Line Chart");
	    //multiRenderer.setXTitle("Week Days");
	    //multiRenderer.setYTitle("Blood Pressure (mm/Hg)");

	    multiRenderer.setXAxisMin(-2);
	    multiRenderer.setXAxisMax(SHOW_MAX_X);
	    multiRenderer.setYAxisMin(0);
	    multiRenderer.setYAxisMax(200);
	    
	    //multiRenderer.setScale(scale);
	    multiRenderer.setLabelsTextSize(30f);
	    multiRenderer.setLegendTextSize(30f);
	    multiRenderer.setAxisTitleTextSize(25f);
	    multiRenderer.setMargins(margins);
	    multiRenderer.setChartTitleTextSize(30f);

	    multiRenderer.setApplyBackgroundColor(true);
	    multiRenderer.setBackgroundColor(getActivity().getResources().getColor(
	            R.color.appdark2bgnd));
	    multiRenderer.setYLabelsAlign(Align.RIGHT);
	    multiRenderer.setBarSpacing(0.5);
	    multiRenderer.setZoomButtonsVisible(false);
	    //multiRenderer.setPanEnabled(false);

	    multiRenderer.setPanEnabled(true, true);
	    multiRenderer.setZoomEnabled(true, true);
	    
	    multiRenderer.setXLabels(0);
	    multiRenderer.setChartTitleTextSize(0f);
	    //multiRenderer.setPanLimits(new double[] { 0, 21, 0, 200});
	    //multiRenderer.setZoomLimits(new double[] {21, 6, 0, 0});
	    multiRenderer.setPanLimits(new double[] { -2, maxPermissableX, 0, 200 });
	    multiRenderer.setZoomLimits(new double[] { -2, maxPermissableX, 0, 200 });

	    multiRenderer.setTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf"));
	    multiRenderer.setShowAxes(false);
	    multiRenderer.setShowGridX(true);
	    multiRenderer.setClickEnabled(true);
	    multiRenderer.setSelectableBuffer(10);
	    multiRenderer.setXLabelsAngle(-20f);
	    //multiRenderer.setXRoundedLabels(true);
	    //multiRenderer.setXLabelsPadding(5f);  
	}

	private void setPulseLineRenderer() {
		pulseLineRenderer = new XYSeriesRenderer();
		pulseLineRenderer.setColor(getActivity().getResources().getColor(
	            R.color.White));
		pulseLineRenderer.setFillPoints(true);
		pulseLineRenderer.setLineWidth(10f);
		pulseLineRenderer.setChartValuesTextAlign(Align.CENTER);
		pulseLineRenderer.setChartValuesTextSize(15f);
		pulseLineRenderer.setDisplayChartValues(false);
		pulseLineRenderer.setPointStyle(PointStyle.CIRCLE);
		pulseLineRenderer.setPointStrokeWidth(25f);
	}
	
	
	//////////////////////////////////////////////////////
	private void setLastBPValueOnScreen(){
		
		CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
        ds.open();
        List<BPData> lst = null;
        lst = ds.getBPValuesForUser(activeProfile.getDbid(), null, " date DESC ");
        ds.close();
        if(lst.size() != 0)
        	displayValueOnScreen(lst.get(0));        
	}
	
	private void displayValueOnScreen(BPData bpd){
		
		final TextView tvs = (TextView) rootView.findViewById(R.id.sysval);
		final TextView tvd = (TextView) rootView.findViewById(R.id.diaval);
		final TextView tvp = (TextView) rootView.findViewById(R.id.hrtbeatval);		
		final TextView tvdate = (TextView)rootView.findViewById(R.id.datetimeval);
		
		tvs.setText(Integer.toString(bpd.getSystolic()));
		
		tvd.setText(Integer.toString(bpd.getDiastolic()));
	
		tvp.setText(Float.toString(bpd.getPulseRate()));
		tvdate.setText(bpd.getDate());
		//("%d-%m-%Y %H:%M:%S")
		SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date tod = null;
		try {
			tod = frm.parse(bpd.getDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat to = new SimpleDateFormat("dd, MMM 'at' HH:mm");
		tvdate.setText(to.format(tod));
		setArrowPosition(bpd.getSystolic(), bpd.getDiastolic());
		
	}
	
	private void showChartFor(String dayrange){
		
		CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
        ds.open();
        List<BPData> lst = null;
        if(dayrange.contains("today")){
        	 lst = ds.getBPValuesForUser(activeProfile.getDbid(), getSQLForToday(), " date DESC ");
        }
        else if(dayrange.contains("week")){
       	 lst = ds.getBPValuesForUser(activeProfile.getDbid(), getSQLForLastWeek(), " date DESC ");
        }
        else if(dayrange.contains("month")){
          	 lst = ds.getBPValuesForUser(activeProfile.getDbid(), getSQLForLastMonth(), " date DESC ");
        }else{
        	lst = ds.getBPValuesForUser(activeProfile.getDbid(), null, null);
        }
        ds.close();
        if(lst.size() == 0) return;
       
        SysSeries.clear(); DiaSeries.clear(); PulseSeries.clear();
        for(int j = 0; j < 100; ++j){
        	multiRenderer.removeXTextLabel(j);//blind
        }
        if(lst.size() <= 4){
        	multiRenderer.setXAxisMax(4);
        	multiRenderer.setPanLimits(new double[] { -2, 6, 0, 200 });
     	    multiRenderer.setZoomLimits(new double[] { -2, 6, 0, 200 });
        }else{
        	multiRenderer.setBarSpacing(0.5);
        	multiRenderer.setXAxisMax(6);
        }
        //int i;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat todf = new SimpleDateFormat("MMM dd");
        for(int j =0, i = (lst.size() >= maxPermissableX ?maxPermissableX:lst.size()) -1 ; i >=0;  ++j,--i){
        	
        	SysSeries.add(j,  lst.get(i).getSystolic());
        	DiaSeries.add(j,  lst.get(i).getDiastolic());
        	PulseSeries.add(j,  lst.get(i).getPulseRate());
        	Date d;
			try {				
				d = df.parse(lst.get(i).getDate());
				multiRenderer.addXTextLabel(j, todf.format(d));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				 mChartView.repaint();
				//mChart.invalidate();	
			}
		}); 
	}
	
	private void populateAllBPData(){
		CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
		ds.open();
		lstbps = ds.getBPValuesForUser(activeProfile.getDbid(), " date DESC ");
		ds.close();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		rootView = inflater.inflate(R.layout.takebpreading, container, false);
		
		//gauge1 = (CustomGauge)rootView.findViewById(R.id.gauge1);
		//gauge2 = (CustomGauge)rootView.findViewById(R.id.gauge2);
		
		arrRedOrng = rootView.findViewById(R.id.arrowred1);
		arrOrng = rootView.findViewById(R.id.arroworange);
		arrGrn = rootView.findViewById(R.id.arrowgreen);
		arrGrnRed = rootView.findViewById(R.id.arrowred2);
		arrFullRed = rootView.findViewById(R.id.arrowred3);
		imgAddManual = (ImageView) rootView.findViewById(R.id.imgaddnew);
		lvBps = (ListView) rootView.findViewById(R.id.lvbplist);
		LinearLayout layout = (LinearLayout)rootView.findViewById(R.id.bpchart);
		activeProfile = ((MainActivity)getActivity()).getActiveProfile();
		
		populateAllBPData();
		//take snapshot
		/*
		 * view.setDrawingCacheEnabled(true);
			Bitmap b = view.getDrawingCache();
			b.compress(CompressFormat.JPEG, 95, new FileOutputStream("/some/location/image.jpg"));
		 */
		SysSeries = new XYSeries("Systolic (mm/Hg)");
		DiaSeries = new XYSeries("Diastolic (mm/Hg)");
		PulseSeries = new XYSeries("Pulse (beats/min)");
	
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		
		dataset.addSeries(SysSeries);
		dataset.addSeries(DiaSeries);
		dataset.addSeries(PulseSeries);
		
		
		setSystolicRenderer();
		setDiastolicRenderer();
		setPulseLineRenderer();
		setMultiRenderer();
	
		multiRenderer.addSeriesRenderer(SysBarRenderer);
		multiRenderer.addSeriesRenderer(DiaBarRenderer);
		multiRenderer.addSeriesRenderer(pulseLineRenderer);
				
		mChartView = ChartFactory.getCombinedXYChartView(getActivity(), dataset,
		        multiRenderer, new String[] { BarChart.TYPE, BarChart.TYPE,
		                LineChart.TYPE });
		
		mChartView.setBackgroundResource(R.color.appdark2bgnd);
		mChartView.setLayoutParams( new ViewGroup.LayoutParams(
		        ViewGroup.LayoutParams.MATCH_PARENT,
		        ViewGroup.LayoutParams.MATCH_PARENT));
		layout.addView(mChartView);		
		
		mChartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();
                
               
                if (seriesSelection != null) {
                	 int seriesIndex = seriesSelection.getSeriesIndex();
                     String selectedSeries="Pulse rate :";
                     if(seriesIndex==0)
                         selectedSeries = "Systolic :";
                     if(seriesIndex==1)
                         selectedSeries = "Diastolic :";
                	
                	Toast.makeText(
                            getActivity(),
                            selectedSeries + seriesSelection.getValue() ,
                            Toast.LENGTH_SHORT).show();
                    /*int seriesIndex = seriesSelection.getSeriesIndex();
                    String selectedSeries="Income";
                    if(seriesIndex==0)
                        selectedSeries = "Income";
                    else
                        selectedSeries = "Expense";
                    // Getting the clicked Month
                    //String month = mMonth[(int)seriesSelection.getXValue()];
                    // Getting the y value
                    int amount = (int) seriesSelection.getValue();
                    */
                }
            }
        });		
		    
		LsDeviceInfo dev = ((MainActivity)getActivity()).getCurrentDeviceToRead();
		
		if(dev.getProtocolType().contains("A3")){
			getActivity().getActionBar().setTitle("  Profile " + dev.getDeviceUserNumber() );
		}else{
			getActivity().getActionBar().setTitle("  Take BP Reading ");
		}
		
		lvBps.setAdapter(new BPListAdapter<BPData>(getActivity(),
				lstbps));		
		
		mManager = ((MainActivity)getActivity()).getBleLeManager();
		if(!mManager.doesSupportBLE()){
			
			Toast.makeText(
                    getActivity(),
                    "Your device isnt compatible. Please enter the BP value",
                    Toast.LENGTH_SHORT).show();
			imgAddManual.setVisibility(View.VISIBLE);
		}
		else{
			mManager.deRegisterAllDevices();
			mManager.registerPairedDevice(dev);
			mManager.receiveData(new CooeyDeviceDataReceiveCallback() {
				
				@Override
				public void onReceiveWeightdata(WeightData wd) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onReceiveBPData(final BPData bpd) {
					// TODO Auto-generated method stub
					mLastKnownBp = bpd;
					lstbps.add(0, bpd);
					((MainActivity)getActivity()).addBPForUser(mLastKnownBp);
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							displayValueOnScreen(bpd);
							
							resetTrendButtons();
							((Button)rootView.findViewById(R.id.btn1d)).setBackgroundResource(R.drawable.grnbrdvwgr2);
							showChartFor("today");				
							
							((BPListAdapter)lvBps.getAdapter()).notifyDataSetChanged();
						}
					});
					
				}
			});
		}
		Button btn1d = (Button)rootView.findViewById(R.id.btn1d);
		btn1d.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetTrendButtons();
				showChartFor("today");	
				v.setBackgroundResource(R.drawable.grnbrdvwgr2);
			}
		});
		
		Button btn1w = (Button)rootView.findViewById(R.id.btn1w);
		btn1w.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetTrendButtons();
				showChartFor("week");	
				v.setBackgroundResource(R.drawable.grnbrdvwgr2);
			}
		});
		
		
		Button btn1m = (Button)rootView.findViewById(R.id.btn1m);
		btn1m.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetTrendButtons();
				showChartFor("month");
				v.setBackgroundResource(R.drawable.grnbrdvwgr2);
			}
		});
		
		final Button btnch = (Button)rootView.findViewById(R.id.btngraph);
		final Button btnlv = (Button)rootView.findViewById(R.id.btnList);
		
		btnlv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lvBps.setVisibility(View.VISIBLE);
				btnch.setBackgroundColor(getActivity().getResources().getColor(R.color.applt2bkgnd));
				btnlv.setBackgroundColor(getActivity().getResources().getColor(R.color.appdarkbgnd));
				//barChart.setVisibility(View.GONE);
			}
		});
		
		btnch.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				btnlv.setBackgroundColor(getActivity().getResources().getColor(R.color.applt2bkgnd));
				btnch.setBackgroundColor(getActivity().getResources().getColor(R.color.appdarkbgnd));
				lvBps.setVisibility(View.GONE);	
				//barChart.setVisibility(View.VISIBLE);
			}
		});
		btn1w.setBackgroundResource(R.drawable.grnbrdvwgr2);
		showChartFor("week");
		setLastBPValueOnScreen();
		
		((MainActivity)getActivity()).changeBluetoothState(true);
		
		return rootView;
	}
	
	private void resetTrendButtons(){
		Button btn1d = (Button)rootView.findViewById(R.id.btn1d);
		Button btn1w = (Button)rootView.findViewById(R.id.btn1w);
		Button btn1m = (Button)rootView.findViewById(R.id.btn1m);
		btn1d.setBackgroundResource(R.drawable.gr2brdvwgr2);
		btn1w.setBackgroundResource(R.drawable.gr2brdvwgr2);
		btn1m.setBackgroundResource(R.drawable.gr2brdvwgr2);
	}
	@Override
	public void onValueSelected(Entry e, int dataSetIndex) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onNothingSelected() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartLongPressed(MotionEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartDoubleTapped(MotionEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartSingleTapped(MotionEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		
	}	

}
