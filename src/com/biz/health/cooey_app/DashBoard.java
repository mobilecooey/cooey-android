package com.biz.health.cooey_app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.SendJsonToCooeyTask;
import com.biz.cooey.SendToCooeyCallbackOnResponse;
import com.biz.cooey.WeightData;
import com.biz.cooey.platform.servermodel.WTServerDataPut;
import com.biz.health.model.BPDashBoard;
import com.biz.health.model.DevDashBoard;
import com.biz.health.model.MedDashBoard;
import com.biz.health.model.MedicineData;
import com.biz.health.model.RecDashBoard;
import com.biz.health.model.RepDashBoard;
import com.biz.health.model.WTDashBoard;
import com.biz.health.utils.db.CooeyBPDataSource;
import com.biz.health.utils.db.CooeyDeviceDataSource;
import com.biz.health.utils.db.CooeyMedDataSource;
import com.biz.health.utils.db.CooeyWTDataSource;
import com.google.gson.GsonBuilder;
import com.lifesense.ble.bean.LsDeviceInfo;

public class DashBoard extends Fragment {

	// private DashboardAdapter myDevAdapter = null;
	private DashboardAdapterNew myDevAdapternw = null;
	private CooeyProfile mActiveProfile = null;
	private LinkedHashMap<String, Object> dashBoardItems = null;
	private List<List<LinkedHashMap<String, Object>>> dashBoardListView = null;
	private Typeface QS_font;
	private Boolean profListShow = false, BPSync = false, WTSync = false, MEDSync = false;
	private ListView lstprofiles = null;
	private Dialog profileListDlg = null;
	private ProfileListAdapter profLstAdapter = null;
	private CooeyProfile currentUserProfile = null;
	private MainActivity context = null; 
	private String defDate = "1970-01-01 00:00:01:0000";
	private int MAXSYNCDAYS = 1, MAXFORCEDSYNCDAYS = 2;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		mActiveProfile = ((MainActivity) activity).getActiveProfile();
		String android_id = Secure.getString(activity.getContentResolver(),
				Secure.ANDROID_ID);
		context = (MainActivity) activity;
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		
		//((MainActivity)getActivity()).setActionBarText("DASHBOARD");
		getActivity().getActionBar().setTitle("DASHBOARD");
		profLstAdapter.notifyDataSetChanged();
		super.onResume();
	}

	public void register(View view) {
		Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
		intent.putExtra("app",
				PendingIntent.getBroadcast(getActivity(), 0, new Intent(), 0));
		intent.putExtra("sender", "naik899@gmail.com");

		getActivity().startService(intent);
	}

	private void loadAllBPData() {
		BPDashBoard bpdata = new BPDashBoard();

		CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
		ds.open();
		// mActiveProfile.getDbid();
		List<BPData> lst = ds.getBPValuesForUser(currentUserProfile.getDbid(), null, " date DESC ");
		ds.close();
		bpdata.count = lst.size();
		if (lst.size() > 0) {
			bpdata.bp = lst.get(0);
			bpdata.bp.setDate(getDateStringInMMMdd(bpdata.bp.getDate()));			
			bpdata.count = lst.size() > 6 ? 6 :lst.size();
		}
		bpdata.lst = lst;
		dashBoardItems.put("BP", bpdata);
	}

	private void loadAllWeightData() {

		WTDashBoard wtdata = new WTDashBoard();

		CooeyWTDataSource ds = new CooeyWTDataSource(getActivity());
		ds.open();
		List<WeightData> lst = ds.getWTValuesForUser(1, " date DESC ");
		ds.close();
		wtdata.count = lst.size();
		if (lst.size() > 0) {
			wtdata.wt = lst.get(0);
			wtdata.wt.setDate(getDateStringInMMMdd(wtdata.wt.getDate()));
		}
		dashBoardItems.put("WT", wtdata);
	}

	private void loadAllDevicesData() {
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(getActivity());
		ds.open();
		List<LsDeviceInfo> lst = ds.getDevicesForUser(1);
		ds.close();
		DevDashBoard dvdata = new DevDashBoard();
		dvdata.count = lst.size();
		dashBoardItems.put("DEV", dvdata);
	}

	private LsDeviceInfo makeDeviceOfType(String type) {

		LsDeviceInfo d1 = new LsDeviceInfo();
		d1.setDeviceName("Cooey device");
		d1.setDeviceType(type);
		d1.setDeviceId(null);// absent
		return d1;
	}

	private void loadAllDevices() {
		DevDashBoard dvdata = new DevDashBoard();
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(getActivity());
		ds.open();
		List<LsDeviceInfo> lst = ds.getDevicesForUser(currentUserProfile.getDbid());
		ds.close();
		LsDeviceInfo d1 = null;
		if (lst.size() > 0) {
			Boolean wtdev = false, bpdev = false;
			for (LsDeviceInfo d : lst) {
				if (CooeyBleDeviceManager.deviceTypeToName(d.getDeviceType())
						.contains("WEIGHT")) {
					wtdev = true;
				}
				if (CooeyBleDeviceManager.deviceTypeToName(d.getDeviceType())
						.contains("BP")) {
					bpdev = true;
				}
			}
			if (bpdev == false) {
				lst.add(makeDeviceOfType("08"));

			}
			if (wtdev == false) {
				lst.add(makeDeviceOfType("01"));
			}
		} else {
			lst.add(makeDeviceOfType("08"));
			lst.add(makeDeviceOfType("01"));
		}
		dvdata.devices = lst;
		dashBoardItems.put("DEV", dvdata);
	}

	private String getDateStringInMMMdd(String from) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d = df.parse(from);
			df = new SimpleDateFormat("MMM dd");
			return df.format(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	private void loadAllMedicineData() {
		CooeyMedDataSource ds = new CooeyMedDataSource(getActivity());
		ds.open();
		List<MedicineData> lst = ds.getMedicinesForUser(currentUserProfile.getDbid(), " date DESC ");
		ds.close();
		MedDashBoard mdb = new MedDashBoard();
		mdb.count = lst.size();
		mdb.md = new ArrayList<MedicineData>();
		if (lst.size() > 0) {
			// mdb.md = lst.get(0);
			// mdb.md.setDate(getDateStringInMMMdd(mdb.md.getDate()));
			mdb.md = new ArrayList<MedicineData>();
			for (MedicineData m : lst) {
				mdb.md.add(m);
			}
		}
		dashBoardItems.put("MED", mdb);
	}

	
	private void loadAllReports() {
		dashBoardItems.put("REP", new RepDashBoard());
	}

	private void loadRecPlaceHolder(){
		dashBoardItems.put("REC", new RecDashBoard());
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.dashboardmenu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.addmedfromdash:
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container,
							((MainActivity) getActivity()).getAddMed())
					.addToBackStack(null).commit();
			break;
		case R.id.adddevfromdash:
			getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container,
							((MainActivity) getActivity()).getAddDevice())
					.addToBackStack(null).commit();
			break;
		case R.id.viewmydevices:
			//getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			getFragmentManager().beginTransaction()
					.replace(R.id.container, new MyDevices())
					.addToBackStack(null).commit();
			break;
		case R.id.viewRecomdfromdash:
			getFragmentManager().beginTransaction()
					.replace(R.id.container, new MyRecommendations())
					.addToBackStack(null).commit();
			break;
		case R.id.viewmymeds:
			getFragmentManager().beginTransaction()
					.replace(R.id.container, new MyMedicines())
					.addToBackStack(null).commit();
			break;
		case R.id.viewmyprof:
			getFragmentManager().popBackStack();
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container,
							((MainActivity) getActivity()).getOnBoard())
					.addToBackStack(null).commit();

			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void showConnectToInternetDialog(){
		
		Dialog connectInternet = new Dialog(context);
		connectInternet.setTitle("Requires internet connection");
		
		connectInternet.show();
	}
	
	
	protected void createNetErrorDialog() {

	    AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    final AlertDialog alert = builder.setMessage("Adding profile requires internet connection.\nPlease turn on mobile network or Wi-Fi in Settings.")
	        .setTitle("No Internet")
	        .setCancelable(false)
	        .setPositiveButton("Settings",
	        new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                Intent i = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
	                startActivity(i);
	            }
	        }
	    )
	    .setNegativeButton("Cancel",
	        new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                dialog.dismiss();
	            }
	        }
	    ).create();
	    alert.show();
	}
	
	private void showProfilesDialog() {

		/*
		 * profileListDlg = new DialogBuilder(getActivity())
		 * .setTitle("Profiles")
		 * .setTitleColor(getActivity().getResources().getColor
		 * (R.color.greenreading)) .setContentView(R.layout.profilelist);
		 * profileListDlg.show();
		 */
		//TODO: get all profiles
		profileListDlg = new Dialog(getActivity());
		// LayoutInflater inflater = profileListDlg.getLayoutInflater();
		// View convertView = (View) inflater.inflate(R.layout.profilelist,
		// null);
		profileListDlg.setContentView(R.layout.profilelist);

		profileListDlg.setTitle("Profiles");
		profileListDlg.getWindow().setBackgroundDrawableResource(
				R.drawable.lt4brdvwlt2);

		int textViewId = profileListDlg.getContext().getResources()
				.getIdentifier("alertTitle", "id", "android");
		if (textViewId != 0) {
		/*	int x = ((ViewGroup)profileListDlg.getWindow().get()).getChildCount();
			for(int i =0; i < x; ++i){
				if(((ViewGroup)profileListDlg.getWindow().getDecorView()).getChildAt(i).getClass() == TextView.class){
					TextView t = (TextView) ((ViewGroup)profileListDlg.getWindow().getDecorView()).getChildAt(i);
					if(t.getText().toString().contains("Profiles"))
					{
						t.setTextColor(getResources().getColor(R.color.greenreading));
						break;
					}
				}
				
			}*/
			TextView tv = (TextView) profileListDlg.findViewById(textViewId);
			if (tv != null) {
				tv.setTextColor(getResources().getColor(R.color.greenreading));
			}
		}

		// profileListDlg.setDividerColor(R.color.greenreading).
		lstprofiles = (ListView) profileListDlg.findViewById(R.id.lstprofiles);
		lstprofiles.setAdapter(profLstAdapter);

		lstprofiles.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				profListShow = false;
				profileListDlg.dismiss();
				TextView txt = (TextView) arg1.findViewById(R.id.txtprofname);
				if(txt == null) return;
				
				if(txt.getText().toString().equalsIgnoreCase("Add new profile") ){
					
					if(!context.haveNetworkConnection()){
						createNetErrorDialog();						
						return;
					}
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							getFragmentManager().popBackStack();
							getFragmentManager()
									.beginTransaction()
									.replace(R.id.container, new OnBoarding(false, true, -1))
									.addToBackStack(null).commit();
						}
					});
				}else if(txt.getText().toString().equalsIgnoreCase("Parent Profile") ){
					context.switchToParent();
				}else{
					context.setCurrentChildProfile((CooeyProfile) profLstAdapter.getItem(arg2));
				}
			}
		});
		profileListDlg.show();
	}

	private Boolean isOutOfSync(String date, String DBdate){
		
		SimpleDateFormat simpleDateFormat = 
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date d1 = null, d2 = null;
		try{
			d1 = simpleDateFormat.parse(date);
			d2 = simpleDateFormat.parse(DBdate);
		}
		catch(Exception e){
			return true;//fail, sync did not happen due to parse error.
		}
		long different = d2.getTime() - d1.getTime();
		long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        //different = different % daysInMilli;
        
        if(Math.abs(elapsedDays) >= MAXSYNCDAYS)
        	return true;
		return false;
	}
	
	private void syncDataPreferences(SharedPreferences prefs){
		
		if(BPSync != true){ //can be null, do not want to sync everytime u come to dashboard.
			String bpdate = prefs.getString("lastBPUpdate", defDate);
			CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
			ds.open();
			List<BPData> lst = ds.getBPValuesForUser(currentUserProfile.getDbid(), null, " date DESC ");
			ds.close();
			BPSync = true;
			if(lst.size() > 0){
				BPSync = !isOutOfSync(bpdate, lst.get(0).getDate());					
			}
		}
		
		if(WTSync != true){
			CooeyWTDataSource ds2 = new CooeyWTDataSource(getActivity());
			ds2.open();
			List<WeightData> lst2 = ds2.getWTValuesForUser(currentUserProfile.getDbid(), null, " date DESC ");
			ds2.close();
			WTSync = true;
			if(lst2.size() > 0){
				String wtdate = prefs.getString("lastWTUpdate", defDate);
				WTSync = !isOutOfSync(wtdate, lst2.get(0).getDate() );
			}
		}
		
		if(MEDSync != true){
			CooeyMedDataSource ds3 = new CooeyMedDataSource(getActivity());
			ds3.open();
			List<MedicineData> lst3 = ds3.getMedicinesForUser(currentUserProfile.getDbid(), " date DESC ");
			ds3.close();
			MEDSync = true;
			if(lst3.size() > 0){		
				String meddate = prefs.getString("lastMEDUpdate", defDate);
				MEDSync = !isOutOfSync(meddate, lst3.get(0).getDate());
			}
		}
		
		if(((MainActivity)getActivity()).haveNetworkConnection()){
			uploadUnSyncedData(prefs);
		}else{
			if(!BPSync ||  !WTSync ){//TODO: add medicine also
				Toast.makeText(getActivity(), " Your data is not synced, please connect to internet.", Toast.LENGTH_LONG).show();
			}
		}
		
	}
	
	private void uploadUnSyncedData(SharedPreferences prefs){
		
		SimpleDateFormat ddmm = 
                new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simpleDateFormat = 
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(!BPSync){
			String tmp = prefs.getString("lastBPUpdate", defDate);
			String s = "";
			try {
				s = ddmm.format(ddmm.parse(tmp));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
			ds.open();
			List<BPData> lst = ds.getBPValuesForUser(currentUserProfile.getDbid(), "DATE(date) > DATE("+s+")", null);
			ds.close();			
			
			for (BPData bpd : lst) {
				
				bpd.setPatientId(currentUserProfile.getPatientId());
				bpd.setTID("vendor:android");
				bpd.setTrackingId("vendor:android");
				try {
					bpd.setTimeStamp( simpleDateFormat.parse(bpd.getDate()).getTime() );
				} catch (ParseException e) {
					e.printStackTrace();
				}
				String mBpJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(bpd);
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {
					
					@Override
					public String getUrl() {
						return "http://api.cooey.co.in/ehealth/v1/actvity/bp";
					}
					
					@Override
					public void callmeWithContext(String resp, Context context) {						
					}
					
					@Override
					public void callme(String resp, int code) {
						
					}
				}).execute(mBpJson);
			}
			BPSync = true;		
			prefs.edit().putString("lastBPUpdate", simpleDateFormat.format(new Date())).commit();
		}
		
		if(!WTSync){
			String tmp = prefs.getString("lastWTUpdate", defDate);
			String s = "";
			try {
				s = ddmm.format(ddmm.parse(tmp));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			CooeyWTDataSource ds2 = new CooeyWTDataSource(getActivity());
			ds2.open();
			List<WeightData> lst2  = ds2.getWTValuesForUser(currentUserProfile.getDbid(), "DATE(date) > DATE("+s+")", null);
			ds2.close();
			
			for (WeightData wd : lst2) {				
				
				WTServerDataPut wtsrv = new WTServerDataPut();
				wtsrv.weight = (float) wd.getWeightKg();
				wtsrv.BMI = (float) wd.getBmi();
				if (wd.getBodyFatRatio() > 0) {
					wtsrv.bodyFat = (float) wd.getBodyFatRatio();
				}
				;
				wtsrv.boneDensity = (float) wd.getBoneDensity();
				wtsrv.muscleMass = (float) wd.getMuscleMassRatio();
				wtsrv.patientId = currentUserProfile.getPatientId();
				wtsrv.TID = "androidapp";
				wtsrv.trackingId = "android";
				try {
					wtsrv.timeStamp = simpleDateFormat.parse(wd.getDate()).getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}				
				wtsrv.totalWater = (float) wd.getBodyWaterRatio();

				String mWtJson = new GsonBuilder()
						.excludeFieldsWithoutExposeAnnotation().create().toJson(wtsrv);
			
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {
					@Override
					public String getUrl() {
						return "http://api.cooey.co.in/ehealth/v1/actvity/weight";
					}

					@Override
					public void callme(String resp, int code) {
					}

					@Override
					public void callmeWithContext(String resp, Context context) {
					}
				}).execute(mWtJson);				
			}
			WTSync = true;		
			prefs.edit().putString("lastWTUpdate", simpleDateFormat.format(new Date())).commit();
		}
		
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.dashboard, container, false);
		final ListView lvw = (ListView) rootView
				.findViewById(R.id.cooeydashbrd);
		TextView profName = (TextView) rootView.findViewById(R.id.txtprofname);
		QS_font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/Quicksand_Light.otf");
		ImageView imgProfile = (ImageView) rootView.findViewById(R.id.imgdashaddprof);
		ImageView profilePic = (ImageView)rootView.findViewById(R.id.imgprof);

		getActivity().getActionBar().show();
		register(rootView);
		setHasOptionsMenu(true);
		
		SharedPreferences myprefs = getActivity().getSharedPreferences("patientPrefs", Context.MODE_PRIVATE);
		
		currentUserProfile = context.getActiveProfile();
		if(context.isUserOnboarding()){
			context.unsetuserOnBoarding();
		}		
		// lstprofiles.setAdapter(new ProfileListAdapter(getActivity(), new
		// ArrayList<CooeyProfile>()));

		dashBoardItems = new LinkedHashMap<String, Object>();
		loadAllDevices();
		loadAllMedicineData();
		loadAllBPData();
		loadAllWeightData();
		loadRecPlaceHolder();
		loadAllReports();
		((MainActivity)getActivity()).loadAllChildProfiles();
		
		profLstAdapter = new ProfileListAdapter(getActivity(),
				((MainActivity)getActivity()).getAllChildProfiles());
		
		profName.setText(((MainActivity) getActivity()).getActiveProfile()
				.getFirstName()
				+ " " + ((MainActivity) getActivity()).getActiveProfile()
						.getLastName());
		profName.setTypeface(QS_font);

		myDevAdapternw = new DashboardAdapterNew(getActivity(),
				R.layout.dashboarditemdevice, dashBoardItems);
		lvw.setAdapter(myDevAdapternw);

		lvw.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,
					long arg3) {

				TextView tmp = (TextView) view.findViewById(R.id.txtmedtext);
				if (tmp != null) {
					dashBoardItems.get("MED");
				}

			}
		});
		imgProfile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (profListShow) {
					profListShow = false;
					profileListDlg.dismiss();
					return;
				}
				profListShow = true;
				showProfilesDialog();
			}
		});
		profilePic.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack();
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.container,
								((MainActivity) getActivity()).getOnBoard())
						.addToBackStack(null).commit();
				
			}
		});
		
		
		/* ImportExport ds = new ImportExport(getActivity()); ds.open(); try {
		  ds.exportDatabase(""); } catch (IOException e) { 
			  // TODO Auto-generated catch block 
			  e.printStackTrace(); }
		*/

		((MainActivity) getActivity())
				.initUsersForNotif(((MainActivity) getActivity())
						.getActiveProfile().getPatientId());
		
		manageSync(myprefs);
		
		return rootView;
	}
	
	private void manageSync(SharedPreferences myprefs){
		
		Editor e = myprefs.edit();
		if (!myprefs.contains("lastBPUpdate"))
		{			
			e.putString("lastBPUpdate", defDate);			
		}
		if (!myprefs.contains("lastWTUpdate"))
		{			
			e.putString("lastWTUpdate", defDate);
		}
		if (!myprefs.contains("lastMEDUpdate"))
		{
			e.putString("lastMEDUpdate", defDate);
		}
		e.commit();
		
		syncDataPreferences(myprefs);
		
	}

	// @Override
	public View onCreateView1(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.dashboard, container, false);
		register(rootView);
		setHasOptionsMenu(true);

		final ListView lv = (ListView) rootView.findViewById(R.id.cooeydashbrd);
		// Button next = (Button)rootView.findViewById(R.id.dshbrdNext);
		dashBoardItems = new LinkedHashMap<String, Object>();
		dashBoardListView = new ArrayList<List<LinkedHashMap<String, Object>>>();

		loadAllMedicineData();
		loadAllBPData();
		loadAllWeightData();
		loadAllDevicesData();

		List<LinkedHashMap<String, Object>> row1 = new ArrayList<LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> x = new LinkedHashMap<String, Object>();
		x.put("MED", dashBoardItems.get("MED"));

		row1.add(x);
		x = new LinkedHashMap<String, Object>();
		x.put("BP", dashBoardItems.get("BP"));
		row1.add(x);

		List<LinkedHashMap<String, Object>> row2 = new ArrayList<LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> x2 = new LinkedHashMap<String, Object>();
		x2.put("WT", dashBoardItems.get("WT"));

		row2.add(x2);
		x2 = new LinkedHashMap<String, Object>();
		x2.put("DEV", dashBoardItems.get("DEV"));
		row2.add(x2);

		dashBoardListView.add(row1);
		dashBoardListView.add(row2);

		// dashBoardItems.put("", null);

		// myDevAdapter = new DashboardAdapter(getActivity(),
		// R.layout.dashbrd_items, dashBoardListView);
		// lv.setAdapter(myDevAdapter);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,
					long arg3) {
				/*
				 * TextView tv = (TextView)view.findViewById(R.id.txtheader);
				 * String item = tv.getText().toString();
				 * if(item.contains("MEDICINES")){
				 * getFragmentManager().beginTransaction
				 * ().replace(R.id.container, new
				 * MyMedicines()).addToBackStack(null).commit(); }else
				 * if(item.contains("DEVICES")){
				 * getFragmentManager().beginTransaction
				 * ().replace(R.id.container, new
				 * MyDevices()).addToBackStack(null).commit(); }
				 */
			}
		});

		// next.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// getFragmentManager().beginTransaction().replace(R.id.container,
		// ((MainActivity)getActivity()).getAddMed()).addToBackStack(null).commit();
		//
		// }
		// });

		return rootView;

	}
}
