package com.biz.health.cooey_app;

import com.google.android.gms.internal.my;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class OneButtonTextDialog extends Dialog {

	String text = "";
	Context ctx = null;
	private Typeface QS_font;
	
	protected OneButtonTextDialog(Context context, String val, myOnClickListener listener){
		super(context);
		text = val;
		QS_font = Typeface.createFromAsset(context.getAssets(),
				"fonts/Quicksand_Book.otf");
		myListener = listener;
    }
 
     
    public myOnClickListener myListener;
         
        // This is my interface //
    public interface myOnClickListener {
        void onButtonClick();
    }
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.onebuttontextview);
 
        TextView tv = (TextView)findViewById(R.id.title);
        TextView tv_common = (TextView)findViewById(R.id.commontext);
        ImageView imgmanual = (ImageView)findViewById(R.id.imgdevmanual);
        
        
        if(text.equalsIgnoreCase("WT")){
        	tv.setText(Html.fromHtml(getContext().getResources().getString(R.string.WT)));
        	tv_common.setText(Html.fromHtml(getContext().getResources().getString(R.string.CommonTextWT)));
        	imgmanual.setBackgroundResource(R.drawable.wtmanual1);
        }
        if(text.equalsIgnoreCase("BP")){
        	tv.setText(Html.fromHtml(getContext().getResources().getString(R.string.BP)));
        	tv_common.setText(Html.fromHtml(getContext().getResources().getString(R.string.CommonTextBP)));
        	imgmanual.setBackgroundResource(R.drawable.bpmanual);
        }
        tv.setTypeface(QS_font);
        tv_common.setTypeface(QS_font);
        
        
        ImageView imgcancel = (ImageView)findViewById(R.id.imgcanceldlg);
        imgcancel.setOnClickListener(new View.OnClickListener(){
			
			@Override
			public void onClick(View v) {
				myListener.onButtonClick();				
			}
		});
       
    }

}
