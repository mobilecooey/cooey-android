package com.biz.health.cooey_app;

import java.text.DecimalFormat;
import java.util.List;

import com.biz.cooey.WeightData;
import com.biz.health.cooey_app.MyMedicinesAdapter.Holder;
import com.biz.health.model.MedicineData;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class WeightListAdapter<T> extends ArrayAdapter<T> {
	
		private List<T> listWgts = null;
		private LayoutInflater inflater = null;
		private int layoutResource;
		private Context ctx;
		private static final int CORNER_RADIUS = 24; // dips
		private static final int MARGIN = 12;
		private float density;
		
		public WeightListAdapter(Context context, List<T> objects) {
			super(context, R.layout.darklistitem, objects);
			ctx = context;
			listWgts = objects;
			layoutResource = R.layout.darklistitem;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			density = ctx.getResources().getDisplayMetrics().density;
			// TODO Auto-generated constructor stub
		}
		
		public int getCount() {
	        return listWgts.size();
	    }
	 
	    public T getItem(int position) {
	        return listWgts.get(position);
	    }
	 
	    public long getItemId(int position) {
	        return position;
	    }
		
	    private double round1(double d){
			DecimalFormat twoDForm = new DecimalFormat("#.#");
			Double t = Double.valueOf(twoDForm.format(d));
			return t;
		}
	    
	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {

	    	Holder holder;
			//view.setBackground(d);
			if(convertView == null)
			{		
				convertView =inflater.inflate(layoutResource, parent, false);

				WeightData wd = (WeightData)getItem(position);
				((TextView) convertView.findViewById(R.id.txtval)).setText(String.valueOf(round1(wd.getWeightKg()))
						+"Kg" +" [BMI "+ String.valueOf(round1(wd.getBmi())) + "]");
				
				((TextView) convertView.findViewById(R.id.txtvaltime)).setText(wd.getDate());
			}
			return convertView;
	    }
}
