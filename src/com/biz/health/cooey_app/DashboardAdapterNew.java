package com.biz.health.cooey_app;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.GetJsonFromCooeyTask;
import com.biz.cooey.SendToCooeyCallbackOnResponse;
import com.biz.cooey.WeightData;
import com.biz.health.cooey_app.OneButtonTextDialog.myOnClickListener;
import com.biz.health.model.BPDashBoard;
import com.biz.health.model.DevDashBoard;
import com.biz.health.model.MedDashBoard;
import com.biz.health.model.RecDashBoard;
import com.biz.health.model.RecommendationData;
import com.biz.health.model.RepDashBoard;
import com.biz.health.model.WTDashBoard;
import com.biz.health.utils.db.CooeyBPDataSource;
import com.biz.health.utils.db.CooeyWTDataSource;
import com.github.mikephil.charting.charts.BarLineChartBase.BorderPosition;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.YLabels;
import com.google.android.gms.internal.bp;
import com.lifesense.ble.bean.LsDeviceInfo;

public class DashboardAdapterNew extends BaseAdapter {

	private LayoutInflater inflater = null;
	private int devlayoutResource, medlayoutResource, replayoutResource,
			reclayoutResource;
	private Context ctx;
	private static final int CORNER_RADIUS = 24; // dips
	private static final int MARGIN = 12;
	private float density;
	private LinkedHashMap<String, Object> dashbrdData;
	private List<Object> allData;
	private String[] mKeys;
	private ArrayList<LineDataSet> wtdataSets = null;
	ArrayList<String> wtxVals = new ArrayList<String>();
	ArrayList<String> bpxVals = new ArrayList<String>();

	ArrayList<Entry> yWts = new ArrayList<Entry>();
	ArrayList<Entry> ySys = new ArrayList<Entry>();
	ArrayList<Entry> yDia = new ArrayList<Entry>();
	private LineData wtLnData = null;
	private ArrayList<LineDataSet> bpdataSets = null;
	private LineData bpLnData = null;
	private Typeface QS_font;
	private LinearLayout linlyt;
	private ImageView medshowmore, medAdd, medList, medBuy, sharerec;
	
	public DashboardAdapterNew(Context context, int resource,
			LinkedHashMap<String, Object> dashBoardItem) {
		ctx = context;
		devlayoutResource = R.layout.dashboarddev;// resource;
		medlayoutResource = R.layout.dashboardmed;
		replayoutResource = R.layout.dashboardrep;
		reclayoutResource = R.layout.dashboardrec;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		density = ctx.getResources().getDisplayMetrics().density;
		dashbrdData = dashBoardItem;
		allData = new ArrayList<Object>();
		for (LsDeviceInfo d : ((DevDashBoard) dashBoardItem.get("DEV")).devices) {
			allData.add(d);
		}
		allData.add(((MedDashBoard) dashBoardItem.get("MED")));
		allData.add(((RecDashBoard) dashBoardItem.get("REC")));
		allData.add(((RepDashBoard) dashBoardItem.get("REP")));

		QS_font = Typeface.createFromAsset(ctx.getAssets(),
				"fonts/Quicksand_Book.otf");
	}

	@Override
	public int getCount() {
		if (allData == null)
			return 0;
		return allData.size();
	}

	@Override
	public Object getItem(int position) {
		// return mData.get(mKeys[position]);
		return allData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private double round1(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		Double t = Double.valueOf(twoDForm.format(d));
		return t;
	}

	private class TakeReadingButtonClickListener implements OnClickListener {

		LsDeviceInfo devType = null;
		int pid = 0;

		public TakeReadingButtonClickListener(LsDeviceInfo dev) {
			devType = dev;
			((MainActivity) ctx).setCurrentDeviceToRead(dev);
		}

		@Override
		public void onClick(View v) {
			final String tmp = CooeyBleDeviceManager.deviceTypeToName(devType
					.getDeviceType());
			((Activity) ctx).runOnUiThread(new Runnable() {
				@Override
				public void run() {

					if (tmp.contains("WEIGHT")) {
						((Activity) ctx)
								.getFragmentManager()
								.beginTransaction()
								.replace(R.id.container,
										((MainActivity) ctx).getTakeWt(),
										"deviceList").addToBackStack(null)
								.commit();
					} else {
						((Activity) ctx)
								.getFragmentManager()
								.beginTransaction()
								.replace(R.id.container,
										((MainActivity) ctx).getTakeBp(),
										"deviceList").addToBackStack(null)
								.commit();
					}
				}
			});

		}
	}

	private void goToUrl(String url) {
		Uri uriUrl = Uri.parse(url);
		Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
		ctx.startActivity(launchBrowser);
	}

	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {

		if (getItem(position) instanceof LsDeviceInfo) {
			if (convertView == null) {
				convertView = inflater
						.inflate(devlayoutResource, parent, false);
				// }

				LsDeviceInfo dev = (LsDeviceInfo) getItem(position);
				ImageView imgvw = (ImageView) convertView
						.findViewById(R.id.imgvwdev);
				LineChart lineChart = (LineChart) convertView
						.findViewById(R.id.dashchartbpwt);
				TextView bpwt = (TextView) convertView
						.findViewById(R.id.txtbpwt);
				TextView txtdate = (TextView) convertView
						.findViewById(R.id.txtbpwtdate);
				TextView txtbmipul = (TextView) convertView
						.findViewById(R.id.dashbmipultxt);
				TextView txtvwbuynow = (TextView) convertView
						.findViewById(R.id.txtvwbuydevice);
				TextView txtvwtakereading = (TextView) convertView
						.findViewById(R.id.txtvwdevtakereading);
				TextView txtvwaddreading = (TextView) convertView
						.findViewById(R.id.txtvwaddmanual);

				bpwt.setTypeface(QS_font);
				txtdate.setTypeface(QS_font);
				txtbmipul.setTypeface(QS_font);

				ImageView imgvwnewdev = (ImageView) convertView
						.findViewById(R.id.imgvwdevaddnew);
				imgvwnewdev.setOnClickListener(null);
				lineChart.setOnClickListener(null);
				txtvwtakereading.setOnClickListener(null);

				// View dev1bkgnd3 = convertView.findViewById(R.id.dev1bkgnd3);
				View dev1bkgnd2 = convertView.findViewById(R.id.dev1bkgnd2);
				View dev1bkgnd5 = convertView.findViewById(R.id.dev1bkgnd5);
				txtvwbuynow.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (((String) v.getTag(v.getId()))
								.equalsIgnoreCase("weight")) {
							StatsUtil.stat_triedBuyWeight(ctx);
						} else {
							StatsUtil.stat_triedBuyBp(ctx);
						}
						goToUrl("http://cooey.co.in");
					}
				});
				imgvwnewdev.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						((Activity) ctx)
								.getFragmentManager()
								.beginTransaction()
								.replace(R.id.container,
										((MainActivity) ctx).getAddDevice())
								.addToBackStack(null).commit();

					}
				});

				// if(dev.getDeviceType() != null){
				String tmp = CooeyBleDeviceManager.deviceTypeToName(dev
						.getDeviceType());
				if (tmp.contains("WEIGHT")) {
					imgvw.setImageBitmap(BitmapFactory.decodeResource(
							ctx.getResources(), R.drawable.scale1));
					txtvwbuynow.setTag(txtvwbuynow.getId(), "weight");
					if (dev.getDeviceId() == null) {
						lineChart.setVisibility(View.GONE);

						bpwt.setText("-- kg");
						txtdate.setText("Date");
						txtbmipul.setText("--\nBMI");
						//txtvwtakereading.setVisibility(View.INVISIBLE);
						txtvwtakereading.setText("How to add?");
						
						txtvwtakereading.setOnClickListener(new OnClickListener() {
							OneButtonTextDialog mydialog;
							@Override
							public void onClick(View v) {
								mydialog = new OneButtonTextDialog(ctx, "WT", new OneButtonTextDialog.myOnClickListener() {
									
									@Override
									public void onButtonClick() {
										mydialog.dismiss();
									}
								} );
						        mydialog.show();
							}							
						});			
					} else {
						prepareChartForType(lineChart, "weights");
						showChartForAllCurrent(lineChart);

						bpwt.setText("-- kg");
						txtbmipul.setText("--\nBMI");
						TakeReadingButtonClickListener takeReadListner = new TakeReadingButtonClickListener(
								dev);
						lineChart.setOnClickListener(takeReadListner);
						txtvwtakereading.setOnClickListener(takeReadListner);

						if (((WTDashBoard) dashbrdData.get("WT")).count != 0) {
							bpwt.setText(String
									.valueOf(round1(((WTDashBoard) dashbrdData
											.get("WT")).wt.getWeightKg()))
									+ " kg");
							txtdate.setText(((WTDashBoard) dashbrdData
									.get("WT")).wt.getDate());
							txtbmipul.setText(String
									.valueOf(round1(((WTDashBoard) dashbrdData
											.get("WT")).wt.getBmi()))
									+ "\nBMI");
						}
					}

				} else if (tmp.contains("BP")) {
					imgvw.setImageBitmap(BitmapFactory.decodeResource(
							ctx.getResources(), R.drawable.bp));
					bpwt.setText("--/--");
					txtdate.setText("Date");
					txtbmipul.setText("--\nbpm");
					txtvwbuynow.setTag(txtvwbuynow.getId(), "bp");
					if (dev.getDeviceId() == null) {
						lineChart.setVisibility(View.GONE);

						//txtvwtakereading.setVisibility(View.INVISIBLE);
						txtvwtakereading.setText("How to add?");
						
						txtvwtakereading.setOnClickListener(new OnClickListener() {
							OneButtonTextDialog mydialog;
							@Override
							public void onClick(View v) {
								mydialog = new OneButtonTextDialog(ctx, "BP", new OneButtonTextDialog.myOnClickListener() {
									
									@Override
									public void onButtonClick() {
										mydialog.dismiss();
									}
								} );
						        mydialog.show();
							}			
						});				        
					} else {
						prepareChartForType(lineChart, "bp");
						showChartForAllCurrentBp(lineChart);
						TakeReadingButtonClickListener takeReadListner = new TakeReadingButtonClickListener(
								dev);
						lineChart.setOnClickListener(takeReadListner);
						txtvwtakereading.setOnClickListener(takeReadListner);

						if (((BPDashBoard) dashbrdData.get("BP")).count != 0) {

							bpwt.setText(String
									.valueOf((int) ((BPDashBoard) dashbrdData
											.get("BP")).bp.getSystolic())
									+ " / "
									+ String.valueOf((int) ((BPDashBoard) dashbrdData
											.get("BP")).bp.getDiastolic()));
							txtdate.setText(((BPDashBoard) dashbrdData
									.get("BP")).bp.getDate());

							txtbmipul.setText(String
									.valueOf(round1(((BPDashBoard) dashbrdData
											.get("BP")).bp.getPulseRate()))
									+ "\nbpm");

						}

						/*
						 * int pixels = (int) (100*density*0.5f); pixels +=
						 * (dev1bkgnd3.getLayoutParams().height +
						 * dev1bkgnd2.getLayoutParams().height+
						 * lineChart.getLayoutParams().height +
						 * dev1bkgnd5.getLayoutParams().height);
						 * RelativeLayout.LayoutParams rel_btn = new
						 * RelativeLayout.LayoutParams(
						 * ViewGroup.LayoutParams.WRAP_CONTENT, pixels);
						 * 
						 * 
						 * RelativeLayout devlyt = (RelativeLayout)
						 * convertView.findViewById(R.id.rlytdev1);
						 * devlyt.setLayoutParams(rel_btn);
						 */
					}
				}
			}

		} else if (getItem(position) instanceof MedDashBoard) {
			// performance: inflation will hurt performance.
			TextView medtxt = null;
			if (convertView == null) {
				convertView = inflater
						.inflate(medlayoutResource, parent, false);

			} else {
				medtxt = (TextView) convertView.findViewById(R.id.txtmedtext);
				if (medtxt == null) {
					convertView = inflater.inflate(medlayoutResource, parent,
							false);
				}
			}

			medtxt = (TextView) convertView.findViewById(R.id.txtmedtext);
			medtxt.setTypeface(QS_font);
			MedDashBoard dbmd = (MedDashBoard) getItem(position);
			linlyt = (LinearLayout) convertView.findViewById(R.id.lytaddnlist);
			medshowmore = (ImageView) convertView
					.findViewById(R.id.imgvwmedshowmore);
			medshowmore.setImageResource(R.drawable.doubledown32);
			medshowmore.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (linlyt.getVisibility() == View.GONE) {
						linlyt.setVisibility(View.VISIBLE);
						medshowmore.setImageResource(R.drawable.doubleup32);

						ListView lv = (ListView) parent
								.findViewById(R.id.cooeydashbrd);
						lv.setSelection(allData.size() - 1);
					} else {
						linlyt.setVisibility(View.GONE);
						medshowmore.setImageResource(R.drawable.doubledown32);
					}
				}
			});
			medAdd = (ImageView) convertView.findViewById(R.id.imgvwmedadd);
			medAdd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					((Activity) ctx)
							.getFragmentManager()
							.beginTransaction()
							.replace(R.id.container,
									((MainActivity) ctx).getAddMed())
							.addToBackStack(null).commit();
				}
			});
			medList = (ImageView) convertView.findViewById(R.id.imgvwmedlist);
			medList.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					((Activity) ctx).getFragmentManager().beginTransaction()
							.replace(R.id.container, new MyMedicines())
							.addToBackStack(null).commit();
				}
			});

		} else if (getItem(position) instanceof RepDashBoard) {

			TextView reptxt = null;
			if (convertView == null) {
				convertView = inflater
						.inflate(replayoutResource, parent, false);

			} else {
				reptxt = (TextView) convertView.findViewById(R.id.txtreptext);
				if (reptxt == null) {
					convertView = inflater.inflate(replayoutResource, parent,
							false);
				}
			}
			reptxt = (TextView) convertView.findViewById(R.id.txtreptext);
			reptxt.setTypeface(QS_font);

		} else if (getItem(position) instanceof RecDashBoard) {

			if (convertView == null) {
				convertView = inflater
						.inflate(reclayoutResource, parent, false);
			}
			final TextView recommendTxt = (TextView) convertView
					.findViewById(R.id.txtrectext);

			ImageView likrec = (ImageView)convertView.findViewById(R.id.imgvwreclike);
			likrec.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					v.setOnClickListener(null);
					v.setBackgroundResource(R.drawable.likerecdone);
					Toast.makeText(ctx, "Liked on Cooey!", Toast.LENGTH_SHORT).show();
				}
			});	
			
			
			sharerec = (ImageView)convertView.findViewById(R.id.imgvwrecshare);
			sharerec.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					String header = "Hey, I would like to share this Tip from Cooey, with you.\n\n" + recommendTxt.getText().toString() + "\n http://cooey.co ";
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, header);
					sendIntent.setType("text/*");
					ctx.startActivity(Intent.createChooser(sendIntent, "Share this tip with.."));					
				}
			});
			
			GetJsonFromCooeyTask recTask = new GetJsonFromCooeyTask(new SendToCooeyCallbackOnResponse() {
				private String url = "http://manage.cooey.co.in/ehealth/v1/suggest/onboard/recommendations/";

				@Override
				public String getUrl() {
					return url
							+ ((MainActivity) ctx).getActiveProfile()
									.getPatientId();
				}

				@Override
				public void callme(String resp, int code) {
					try {
						JSONObject jo = new JSONObject(resp);
						// ask the server to send empty array, not null.
						if (jo.isNull("recommendations")) {
							return;
						}
						JSONArray recs = (JSONArray) jo.get("recommendations");
						final JSONObject jorec = (JSONObject) recs.get(recs
								.length() - 1);
						
						((Activity) ctx).runOnUiThread(new Runnable() {

							@Override
							public void run() {
								try {
									recommendTxt.setText(jorec
											.getString("text"));
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								recommendTxt.setTypeface(QS_font);
								sharerec.setVisibility(View.VISIBLE);
							}
						});

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				@Override
				public void callmeWithContext(String resp, Context context) {
					// TODO Auto-generated method stub
					
				}
			});
			recTask.execute("");
		}

		return convertView;
	}

	public void initializeLineDataSetForPlotting() {

		// yWts.clear(); xVals.clear();
		// yWts.add(new Entry((float) 70.0, 0));
		// xVals.add(" ");

		LineDataSet set1 = new LineDataSet(yWts, "(Kgs)");
		set1.setLineWidth(2f);
		set1.setCircleSize(4f);
		set1.setFillAlpha(65);
		set1.setFillColor(ColorTemplate.getHoloBlue());
		set1.setHighLightColor(Color.rgb(244, 117, 117));

		wtdataSets = new ArrayList<LineDataSet>();
		wtdataSets.add(set1); // add the datasets
		set1.setColor(ctx.getResources().getColor(R.color.DarkGreen));
		set1.setCircleColor(ctx.getResources().getColor(R.color.White));
		// create a data object with the datasets
		wtLnData = new LineData(wtxVals, wtdataSets);
	}

	private void prepareChartForType(LineChart mChart, String type) {
		mChart.setOnChartGestureListener(null);
		mChart.setOnChartValueSelectedListener(null);
		mChart.setValueTextColor(Color.WHITE);

		mChart.setUnit("");
		mChart.setDrawUnitsInChart(true);

		// if enabled, the chart will always start at zero on the y-axis
		mChart.setStartAtZero(false);

		// disable the drawing of values into the chart
		mChart.setDrawYValues(false);
		mChart.setDrawXLabels(false);
		mChart.setDrawBorder(true);
		mChart.setBorderPositions(new BorderPosition[] { BorderPosition.BOTTOM });

		// no description text
		mChart.setDescription("");
		mChart.setNoDataText("");

		mChart.setNoDataTextDescription("");

		// enable value highlighting
		mChart.setHighlightEnabled(true);
		mChart.setMaxVisibleValueCount(5);

		// enable touch gestures
		mChart.setTouchEnabled(true);

		// enable scaling and dragging
		mChart.setDragEnabled(true);
		mChart.setScaleEnabled(true);
		mChart.setDrawGridBackground(false);
		mChart.setDrawVerticalGrid(false);
		mChart.setDrawHorizontalGrid(true);

		// if disabled, scaling can be done on x- and y-axis separately
		mChart.setPinchZoom(false);
		mChart.setTouchEnabled(false);
		mChart.setValueTypeface(QS_font);

	}

	private void showChartForAllCurrent(final LineChart mChart) {
		CooeyWTDataSource ds = new CooeyWTDataSource(ctx);
		ds.open();
		List<WeightData> lst = ds.getWTValuesForUser(1, null);
		ds.close();

		if (lst.size() == 0) {
			mChart.setNoDataTextDescription("Take you first Weight and start monitoring");
			mChart.setNoDataText("It's time to step up on the scale");
			return;
		}
		yWts.clear();
		wtxVals.clear();
		for (int i = 0; i < lst.size(); ++i) {

			yWts.add(new Entry((float) lst.get(i).getWeightKg(), i));
			wtxVals.add(lst.get(i).getDate());
		}

		if (wtLnData == null) {
			initializeLineDataSetForPlotting();
		}
		mChart.setData(wtLnData);

		XLabels xl = mChart.getXLabels();
		xl.setTextColor(ctx.getResources().getColor(R.color.WhiteSmoke));
		xl.setTypeface(QS_font);

		YLabels yl = mChart.getYLabels();
		yl.setTextColor(ctx.getResources().getColor(R.color.WhiteSmoke));
		yl.setLabelCount(5);
		yl.setTypeface(QS_font);

		mChart.getLegend().setTextColor(
				ctx.getResources().getColor(R.color.White));
		mChart.getLegend().setTypeface(QS_font);

		((Activity) ctx).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// mChart.setYRange(min - 20, max + 20, true);
				mChart.animateX(1000);
				// mChart.invalidate();
			}
		});
	}

	private void initializeLineDataSetForBpPlotting() {

		// yWts.clear(); xVals.clear();
		// yWts.add(new Entry((float) 70.0, 0));
		// xVals.add(" ");

		LineDataSet set1 = new LineDataSet(ySys, "(Sys)");
		set1.setColor(ColorTemplate.getHoloBlue());
		set1.setCircleColor(ColorTemplate.getHoloBlue());
		set1.setLineWidth(2f);
		set1.setCircleSize(4f);
		set1.setFillAlpha(65);
		set1.setFillColor(ColorTemplate.getHoloBlue());
		set1.setHighLightColor(Color.rgb(244, 117, 117));
		set1.setColor(ctx.getResources().getColor(R.color.orangereading));
		set1.setCircleColor(ctx.getResources().getColor(R.color.White));

		LineDataSet set2 = new LineDataSet(yDia, "(Dia)");
		set2.setColor(ColorTemplate.getHoloBlue());
		set2.setCircleColor(ColorTemplate.getHoloBlue());
		set2.setLineWidth(2f);
		set2.setCircleSize(4f);
		set2.setFillAlpha(65);
		set2.setFillColor(ColorTemplate.getHoloBlue());
		set2.setHighLightColor(Color.rgb(244, 117, 117));
		set2.setColor(ctx.getResources().getColor(R.color.greenreading));
		set2.setCircleColor(ctx.getResources().getColor(R.color.Yellow));

		bpdataSets = new ArrayList<LineDataSet>();
		bpdataSets.add(set1);
		bpdataSets.add(set2);
		// create a data object with the datasets
		bpLnData = new LineData(bpxVals, bpdataSets);
	}

	private void showChartForAllCurrentBp(final LineChart mChart) {

		/*
		 * CooeyBPDataSource ds = new CooeyBPDataSource(ctx); ds.open();
		 * List<BPData> lst = ds.getBPValuesForUser(1, null, null); ds.close();
		 */

		BPDashBoard bpd = (BPDashBoard) dashbrdData.get("BP");
		List<BPData> lst = bpd.lst;
		if (lst.size() == 0) {
			mChart.setNoDataTextDescription("Take you first BP reading and start monitoring");
			mChart.setNoDataText("It's time to measure your Blood Pressure");
			return;
		}

		for (int i = bpd.count - 1, j = 0; i >= 0; --i, ++j) {
			ySys.add(new BarEntry(lst.get(i).getSystolic(), j));
			yDia.add(new BarEntry(lst.get(i).getDiastolic(), j));
			bpxVals.add(lst.get(i).getDate());
		}

		if (bpLnData == null) {
			initializeLineDataSetForBpPlotting();
		}
		mChart.setData(bpLnData);

		XLabels xl = mChart.getXLabels();
		xl.setTextColor(ctx.getResources().getColor(R.color.WhiteSmoke));
		xl.setTypeface(QS_font);

		YLabels yl = mChart.getYLabels();
		yl.setTextColor(ctx.getResources().getColor(R.color.WhiteSmoke));
		yl.setLabelCount(5);
		yl.setTypeface(QS_font);

		mChart.getLegend().setTextColor(
				ctx.getResources().getColor(R.color.White));
		mChart.getLegend().setTypeface(QS_font);

		((Activity) ctx).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// mChart.setYRange(min - 20, max + 20, true);
				mChart.animateX(1000);
				// mChart.invalidate();
			}
		});
	}
}
