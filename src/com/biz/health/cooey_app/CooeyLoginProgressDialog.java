package com.biz.health.cooey_app;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class CooeyLoginProgressDialog extends AsyncTask<String, Void, Boolean> {

	private String uName = "", uPass = "";
	String extId = "0";
	private Context context;
	private String lastErrorMessage = "";
	private JSONObject successJo = null;
	private Boolean isUserOnboarding = false;

	public CooeyLoginProgressDialog(Context ctx, Boolean isOnboarding) {
		this.context = ctx;
		dialog = new ProgressDialog(ctx);
		isUserOnboarding = isOnboarding;
	}

	/** progress dialog to show user that the backup is processing. */
	private ProgressDialog dialog;

	protected void onPreExecute() {
		this.dialog.setMessage("Logging into Cooey");
		this.dialog.show();
		this.dialog.setCancelable(false);
	}

	@Override
	protected void onPostExecute(final Boolean success) {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		if (!success) {
			Toast.makeText(context, "Failed to login\n" + lastErrorMessage,
					Toast.LENGTH_SHORT).show();
		} else {
			// get extId and send to mainactivity
			ResponseProfile rp = new ResponseProfile();
			rp.setEmail(uName);
			rp.setId(extId);
			rp.setFirstname("firstname");
			rp.setLastname("");
			try {
				rp.setFirstname((String) successJo.get("firstName"));
				rp.setLastname((String) successJo.get("lastName"));
			} catch (Exception e) {
				Toast.makeText(context, "Error, Unable to retrieve.",
						Toast.LENGTH_SHORT).show();
				return;
			}
			((MainActivity) this.context).onCustomLoginResult(rp, isUserOnboarding);
		}
	}

	private Boolean verifyUserCredentials(String url) {

		DefaultHttpClient httpClient = null;
		try {
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
			HttpConnectionParams.setSoTimeout(httpParameters, 5000);
			httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(url);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			int responseCode = httpResponse.getStatusLine().getStatusCode();
			String responseBody = null;

			switch (responseCode) {
			case 200:
				HttpEntity entity = httpResponse.getEntity();
				if (entity == null)
					return false;

				responseBody = EntityUtils.toString(entity);
				successJo = new JSONObject(responseBody);
				extId = (String) successJo.get("externalId");
				if (extId.equalsIgnoreCase("0")) {
					lastErrorMessage = "Wrong credentials.";
					return false;
				}
				return true;
			default:
				lastErrorMessage = "Server error";
				return false;
			}

		} catch (ConnectTimeoutException e) {
			lastErrorMessage = "Connection timeout.";
			httpClient.getConnectionManager().shutdown();
			return false;

		} catch (Exception e) {
			lastErrorMessage = "Error: " + e.getMessage();
			Log.e("tag", "error", e);
			httpClient.getConnectionManager().shutdown();
			return false;
		}

	}

	protected Boolean doInBackground(final String... args) {

		String url = "http://manage.cooey.co.in/ehealth/v1/profile/cooey/account/?";
		url += "email=" + uName + "&password=" + uPass.replace(" ", "%20");
		url = url.trim();
		return verifyUserCredentials(url);
	}

	public void setLoginParams(String uname, String pass) {
		uName = uname;
		uPass = pass;
	}

}
