package com.biz.health.cooey_app;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.biz.health.model.MedicineData;
import com.lifesense.ble.bean.LsDeviceInfo;

public class MyMedicines extends Fragment {

	private List<MedicineData> currentMeds = null;
	private MyMedicinesAdapter<MedicineData> myMedAdapter = null;
	
	public MyMedicines()
	{
		//TODO
		//move all Fragments to singletons
		
	}
	
	private void setDefaultActionBarTitle(){
		getActivity().getActionBar().setTitle("MY MEDICINES");
	}
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		setDefaultActionBarTitle();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setDefaultActionBarTitle();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.my_medicines, container,
				false);
		
		ListView myMedList = (ListView)rootView.findViewById(R.id.MyMedLst);
		Button back = (Button)rootView.findViewById(R.id.btnBack);

		currentMeds = new ArrayList<MedicineData>();
		List<MedicineData>lst = ((MainActivity)getActivity()).getMedicinesForCurrentUser();
		
		if(lst.size() == 0){
			Toast.makeText(getActivity(), "You have no medicines added.", Toast.LENGTH_SHORT).show();
		}
		else{
			for(MedicineData d : lst){
				currentMeds.add(d);
			}
		}
		myMedAdapter = new MyMedicinesAdapter<MedicineData>(getActivity(), R.layout.my_medlistitem, currentMeds);
		myMedList.setAdapter(myMedAdapter);
		
		back.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Fragment f = ((MainActivity)getActivity()).getDashboard();
				getFragmentManager().beginTransaction().replace(R.id.container, f, "").addToBackStack(null).commit();
			}
		});
		
		return rootView;
	}
	
}

