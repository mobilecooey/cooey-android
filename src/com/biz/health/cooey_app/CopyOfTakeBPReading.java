package com.biz.health.cooey_app;

import java.security.acl.LastOwnerException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.Time;
import android.text.method.DateTimeKeyListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyDeviceDataReceiveCallback;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.WeightData;
import com.biz.health.utils.db.CooeyBPDataSource;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.BarLineChartBase.BorderPosition;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.OnChartGestureListener;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.LargeValueFormatter;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.XLabels.XLabelPosition;
import com.github.mikephil.charting.utils.YLabels;
import com.github.mikephil.charting.utils.YLabels.YLabelPosition;
import com.lifesense.ble.bean.LsDeviceInfo;

public class CopyOfTakeBPReading extends Fragment implements OnChartGestureListener, OnChartValueSelectedListener {
	
	private CooeyBleDeviceManager mManager = null;
	//private Button mSave = null, mTakeAgain = null;
	private BPData mLastKnownBp = null;
	//private GuageView gv = null;
	private CustomGauge gauge1;
	private CustomGauge gauge2;
	private BarData mBPdata = null;
	private BarChart mChart;
	ArrayList<BarEntry> ySys = new ArrayList<BarEntry>();
    ArrayList<BarEntry> yDia = new ArrayList<BarEntry>();
    ArrayList<BarEntry> yPul = new ArrayList<BarEntry>();
    ArrayList<String> xVals = new ArrayList<String>();
    ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
    private View rootView;
    private View arrRedOrng, arrOrng, arrGrn, arrGrnRed, arrFullRed;
    
	private class sydia{
		private Float sys;
		private Float dia;
		private String date;
		public Float getSys() {
			return sys;
		}
		public void setSys(Float sys) {
			this.sys = sys;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public Float getDia() {
			return dia;
		}
		public void setDia(Float dia) {
			this.dia = dia;
		}
		
	}
	
	private ArrayList<sydia> mydata = new ArrayList<CopyOfTakeBPReading.sydia>();
	
	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
	}
	@Override
	public void onDestroyView() {
	
		super.onDestroyView();
		mManager.shutdown();
		((MainActivity)getActivity()).changeBluetoothState(false);
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//mManager.shutdown();
	}
	
	private void prepareChart(View rootView){
		mChart = (BarChart) rootView.findViewById(R.id.bpchart);
		 mChart.setOnChartGestureListener(this);
		 mChart.setOnChartValueSelectedListener(this);
		 mChart.setDrawUnitsInChart(true);
		 // if enabled, the chart will always start at zero on the y-axis
		 mChart.setStartAtZero(false);
		 // disable the drawing of values into the chart
		 mChart.setDrawYValues(true);
		 mChart.setDrawValueAboveBar(true);
		 mChart.setDescription("");
		 // if more than 60 entries are displayed in the chart, no values will be
		 // drawn
		 mChart.setMaxVisibleValueCount(12);
		 		 // disable 3D
		 //mChart.set3DEnabled(true);
		 // scaling can now only be done on x- and y-axis separately
		 mChart.setPinchZoom(true);
		 // draw shadows for each bar that show the maximum value
		 // mChart.setDrawBarShadow(true);
		 //mChart.setUnit("mm/Hg");
		 // mChart.setDrawXLabels(false);
		 mChart.setDrawGridBackground(false);
		 mChart.setDrawHorizontalGrid(true);
		 mChart.setDrawVerticalGrid(false);
		 // mChart.setDrawYLabels(false);
		 // sets the text size of the values inside the chart
		 mChart.setValueTextSize(10f);
		 mChart.setDrawBorder(false);
		 mChart.setNoDataTextDescription("No BP data available yet.");
		 
		 //mChart.set
		 //mChart.setValueFormatter(new LargeValueFormatter());
		 // mChart.setBorderPositions(new BorderPosition[] {BorderPosition.LEFT,
		 // BorderPosition.RIGHT});
		 /// Typeface tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
		 XLabels xl = mChart.getXLabels();
		 xl.setPosition(XLabelPosition.BOTTOM);
		 xl.setCenterXLabelText(true);
		 xl.setTextColor(getActivity().getResources().getColor(R.color.White));
		 
		 //xl.setTypeface(tf);
		 YLabels yl = mChart.getYLabels();
		 yl.setTextColor(getActivity().getResources().getColor(R.color.White));
		 //yl.setTypeface(tf);
		 yl.setLabelCount(6);
		 //yl.setPosition(YLabelPosition.LEFT);		 
		 //mChart.setValueTypeface(tf);	 
		 mChart.setDrawBarShadow(false);
	}
	/*
	 * Blood Pressure
		Category 					Systolic 				Diastolic
		Normal 						less than 120 	and 	less than 80
		Prehypertension 			120 � 139 	or 	80 � 89
		High Blood Pressure
		(Hypertension) Stage 1 		140 � 159 	or 	90 � 99
		High Blood Pressure
		(Hypertension) Stage 2 		160 or higher 	or 	100 or higher
		Hypertensive Crisis
		(Emergency care needed) 	Higher than 180 	or 	Higher than 110

	 */
	private int getZoneForBPSys(int sys){

		if(sys < 120){ return 2;}
		if(sys > 120 && sys <= 139) { return 1; }
		if(sys > 139 && sys <= 159) { return 0; }
		if(sys > 159 && sys <= 179) { return 3; }
		//if(sys > 179) { return 4; }
		return 4;
		//return color;	
	}
	
	private int getZoneForBPDia(int dia){
		if(dia < 80){ return 2; }
		if(dia > 80 && dia <= 89) { return 1; }
		if(dia > 89 && dia <= 99) { return 2; }
		if(dia > 99 && dia <= 109) { return 3; }
		//if(dia > 109) { return R.color.Red; }
		return 4;
	}

	private void showChartForAll(){
		
		CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
        ds.open();
        List<BPData> lst = ds.getBPValuesForUser(1, null, null);
        ds.close();
        ySys.clear(); yDia.clear(); yPul.clear(); xVals.clear();
        
        for(int i =0; i < lst.size(); ++i){
        	ySys.add(new BarEntry(lst.get(i).getSystolic(), i));
        	yDia.add(new BarEntry(lst.get(i).getDiastolic(), i));
        	yPul.add(new BarEntry(lst.get(i).getPulseRate(), i));
        	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");     	
        	try {
        		Date d = df.parse(lst.get(i).getDate());
        		df = new SimpleDateFormat("MMM dd");
        		xVals.add(df.format(d));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        	//xVals.add(lst.get(i).getDate());
        }
        //mBPdata.removeDataSet(d);
        //mBPdata.addDataSet(d);
        mChart.notifyDataSetChanged();
		//mChart.setData(mBPdata);
	    mChart.invalidate();		
	}
	
	private void showChartForNone(){
		ySys.clear(); yDia.clear(); yPul.clear(); xVals.clear();
		mChart.setData(mBPdata);
	    mChart.invalidate();	
	}
	
	private String getSQLForToday(){
		return " date(date, 'start of day') = date('now', 'start of day') ";
	}
	
	private String getSQLForLastWeek(){
		return " date(date, 'start of day') >= date('now', '-7 days') ";
	}
	
	private String getSQLForLastMonth(){
		return " date(date, 'start of day') >= date('now', '-30 days') ";//start of month
	}
	
	private void showChartFor(String dayrange){
		
		CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
        ds.open();
        List<BPData> lst = null;
        if(dayrange.contains("today")){
        	 lst = ds.getBPValuesForUser(1, getSQLForToday(), null);
        }
        else if(dayrange.contains("week")){
       	 lst = ds.getBPValuesForUser(1, getSQLForLastWeek(), null);
        }
        else if(dayrange.contains("month")){
          	 lst = ds.getBPValuesForUser(1, getSQLForLastMonth(), null);
        }else{
        	lst = ds.getBPValuesForUser(1, null, null);
        }
        ds.close();
        if(lst.size() == 0) return;
        ySys.clear(); yDia.clear(); yPul.clear(); xVals.clear();
        
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        int i;
        for(i =0; i < lst.size(); ++i){
        	ySys.add(new BarEntry(lst.get(i).getSystolic(), i));
        	yDia.add(new BarEntry(lst.get(i).getDiastolic(), i));
        	yPul.add(new BarEntry(lst.get(i).getPulseRate(), i));        	   	
        	try {
        		Date d = df.parse(lst.get(i).getDate());
        		df = new SimpleDateFormat("MMM dd");
        		xVals.add(df.format(d));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        /*ySys.add(new BarEntry(38, i));
    	yDia.add(new BarEntry(39, i));
    	xVals.add(df.format(new Date()));
    	++i;
    	ySys.add(new BarEntry(40, i));
    	yDia.add(new BarEntry(50, i));
    	xVals.add(df.format(new Date()));*/
    	
		mChart.setData(mBPdata);
		//mChart.setScaleMinima(0.2f, 1f);
		if(lst.size() >= 4){
			//mChart.setScaleMinima(2f, 1f);
			//mChart.centerViewPort(lst.size(), 180f );
		}
		mChart.getLegend().setTextColor(getActivity().getResources().getColor(R.color.White));
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				 mChart.animateY(1500);
				//mChart.invalidate();	
			}
		}); 
	}
	
	private void initializeBarDataSetForPlotting(){
		
        ySys.clear(); yDia.clear(); yPul.clear(); xVals.clear();
        
    	ySys.add(new BarEntry((float)180.0, 0));
    	yDia.add(new BarEntry((float)50.0, 0));
    	yPul.add(new BarEntry((float)75.0, 0));
    	xVals.add("");
    	
		BarDataSet set1 = new BarDataSet(ySys, "Systolic (mm/Hg)");
		set1.setColor(getActivity().getResources().getColor(R.color.orangereading)); //Color.rgb(104, 241, 175));
		set1.setBarSpacePercent(5f);
		BarDataSet set2 = new BarDataSet(yDia, "Diastolic (mm/Hg)");
		set2.setColor(getActivity().getResources().getColor(R.color.greenreading));
		set2.setBarSpacePercent(5f);
		BarDataSet set3 = new BarDataSet(yPul, "Pulse (beats/min)");
		set3.setColor(Color.rgb(242, 247, 158));
		set3.setBarSpacePercent(5f);
		
		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);
		dataSets.add(set2);
		dataSets.add(set3);
		
		mBPdata = new BarData(xVals, dataSets);
		
		// add space between the dataset groups in percent of bar-width
		mBPdata.setGroupSpace(50f);			
	}
	
	private void setVisibilityOf(View v, int visibility){
		
		arrRedOrng.setVisibility(View.INVISIBLE);
		arrOrng.setVisibility(View.INVISIBLE);
		arrGrn.setVisibility(View.INVISIBLE);
		arrGrnRed.setVisibility(View.INVISIBLE);
		arrFullRed.setVisibility(View.INVISIBLE);
		v.setVisibility(visibility);
		
	}
	private void setArrowPosition(int s, int d){
		
		int zone_sys = getZoneForBPSys(s);
		int zone_dia = getZoneForBPDia(d);
		if(zone_dia == 2 && zone_sys == 2){
			//arrowgreen
			setVisibilityOf(arrGrn, View.VISIBLE);
		}else{
			if(zone_dia == 1 || zone_sys == 1){
				//green n orange
				//arroworange
				setVisibilityOf(arrOrng, View.VISIBLE);
				return;
			}
			if(zone_dia == 0 || zone_sys == 0){
				//ornage
				//arrowred1
				setVisibilityOf(arrRedOrng, View.VISIBLE);
				return;
			}
			if(zone_dia == 3 || zone_sys == 3){
				//green n red
				//arrowred2
				setVisibilityOf(arrGrnRed, View.VISIBLE);
				return;
			}
			if(zone_dia == 4 || zone_sys == 4){
				//full red
				//arrowred3
				setVisibilityOf(arrFullRed, View.VISIBLE);
				return;
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		rootView = inflater.inflate(R.layout.takebpreading, container, false);
		final TextView tvs = (TextView) rootView.findViewById(R.id.sysval);
		final TextView tvd = (TextView) rootView.findViewById(R.id.diaval);
		final TextView tvp = (TextView) rootView.findViewById(R.id.hrtbeatval);
		final BarChart barChart = (BarChart) rootView.findViewById(R.id.bpchart);
		final ListView lvBps = (ListView) rootView.findViewById(R.id.lvbplist);
		final TextView tvdate = (TextView)rootView.findViewById(R.id.datetimeval);
		
		//gauge1 = (CustomGauge)rootView.findViewById(R.id.gauge1);
		//gauge2 = (CustomGauge)rootView.findViewById(R.id.gauge2);
		
		arrRedOrng = rootView.findViewById(R.id.arrowred1);
		arrOrng = rootView.findViewById(R.id.arroworange);
		arrGrn = rootView.findViewById(R.id.arrowgreen);
		arrGrnRed = rootView.findViewById(R.id.arrowred2);
		arrFullRed = rootView.findViewById(R.id.arrowred3);
		
		prepareChart(rootView);
		initializeBarDataSetForPlotting();
		showChartFor("month");
		
		LsDeviceInfo dev = ((MainActivity)getActivity()).getCurrentDeviceToRead();
		
		getActivity().getActionBar().setTitle(dev.getDeviceName());
		
		mManager = ((MainActivity)getActivity()).getBleLeManager();
		mManager.deRegisterAllDevices();
		mManager.registerPairedDevice(dev);		
		mManager.receiveData(new CooeyDeviceDataReceiveCallback() {
			
			@Override
			public void onReceiveWeightdata(WeightData wd) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onReceiveBPData(final BPData bpd) {
				// TODO Auto-generated method stub
				mLastKnownBp = bpd;
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						tvs.setText(Integer.toString(bpd.getSystolic()));
						
						tvd.setText(Integer.toString(bpd.getDiastolic()));
					
						tvp.setText(Float.toString(bpd.getPulseRate()));
						tvdate.setText(bpd.getDate());
						//("%d-%m-%Y %H:%M:%S")
						SimpleDateFormat frm = new SimpleDateFormat("dd-mm-yyyy HH:mm:ss");
						Date tod = null;
						try {
							tod = frm.parse(bpd.getDate());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						SimpleDateFormat to = new SimpleDateFormat("dd, MMM 'at' HH:mm");
						tvdate.setText(to.format(tod));
						setArrowPosition(bpd.getSystolic(), bpd.getDiastolic());
					}
				});
				((MainActivity)getActivity()).addBPForUser(mLastKnownBp);
				showChartFor("today");
			}
		});
		
		Button btn1d = (Button)rootView.findViewById(R.id.btn1d);
		btn1d.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showChartFor("today");							
			}
		});
		
		Button btn1w = (Button)rootView.findViewById(R.id.btn1w);
		btn1w.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showChartFor("week");						
			}
		});
		
		Button btn1m = (Button)rootView.findViewById(R.id.btn1m);
		btn1m.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showChartFor("week");				
			}
		});
		
		final Button btnch = (Button)rootView.findViewById(R.id.btngraph);
		final Button btnlv = (Button)rootView.findViewById(R.id.btnList);
		
		btnlv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lvBps.setVisibility(View.VISIBLE);
				btnch.setBackgroundColor(getActivity().getResources().getColor(R.color.applt2bkgnd));
				btnlv.setBackgroundColor(getActivity().getResources().getColor(R.color.appdarkbgnd));
				barChart.setVisibility(View.GONE);
			}
		});
		
		btnch.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				btnlv.setBackgroundColor(getActivity().getResources().getColor(R.color.applt2bkgnd));
				btnch.setBackgroundColor(getActivity().getResources().getColor(R.color.appdarkbgnd));
				lvBps.setVisibility(View.GONE);	
				barChart.setVisibility(View.VISIBLE);
			}
		});
		((MainActivity)getActivity()).changeBluetoothState(true);
		return rootView;
	}
	
	
	
	public BarData prepareBarDataSetForPlotting1(){
        CooeyBPDataSource ds = new CooeyBPDataSource(getActivity());
        ds.open();
        List<BPData> lst = ds.getBPValuesForUser(1, null, null);
        ds.close();
        
        for(int i =0; i < lst.size(); ++i){
        	ySys.add(new BarEntry(lst.get(i).getSystolic(), i));
        	yDia.add(new BarEntry(lst.get(i).getDiastolic(), i));
        	yPul.add(new BarEntry(lst.get(i).getPulseRate(), i));
        	xVals.add(lst.get(i).getDate());
        }
       
        
        BarDataSet set1 = new BarDataSet(ySys, "Systolic (mm/Hg)");
        set1.setColor(Color.rgb(104, 241, 175));
        set1.setBarSpacePercent(5f);
        BarDataSet set2 = new BarDataSet(yDia, "Diastolic (mm/Hg)");
        set2.setColor(Color.rgb(164, 228, 251));
        set2.setBarSpacePercent(5f);
        BarDataSet set3 = new BarDataSet(yPul, "Pulse (beats/min)");
        set3.setColor(Color.rgb(242, 247, 158));
        set3.setBarSpacePercent(5f);
        
        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);
        dataSets.add(set2);
        dataSets.add(set3);

        mBPdata = new BarData(xVals, dataSets);
        
        // add space between the dataset groups in percent of bar-width
        mBPdata.setGroupSpace(50f);
        
        //mChart.centerViewPort(2, 1f);
        return mBPdata;
	}
	
	private void setData(int count, float range) {
		ArrayList<String> xVals = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			xVals.add("val"+i);
		}
		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
		for (int i = 0; i < count; i++) {
			float mult = (range + 1);
			float val = (float) (Math.random() * mult);
			yVals1.add(new BarEntry(val, i));
		}
		BarDataSet set1 = new BarDataSet(yVals1, "DataSet");
		set1.setBarSpacePercent(35f);
		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);
		BarData data = new BarData(xVals, dataSets);
		mChart.setData(data);
	}
	
	
	@Override
	public void onChartLongPressed(MotionEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartDoubleTapped(MotionEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartSingleTapped(MotionEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onValueSelected(Entry e, int dataSetIndex) {
		// TODO Auto-generated method stub
		if(e == null) return;
		if(dataSetIndex == 0){
			Toast.makeText(getActivity(), "Your Systolic was "+e.getVal(), Toast.LENGTH_SHORT).show();
		}
		else if(dataSetIndex == 1){
			Toast.makeText(getActivity(), "Your Diastolic was "+e.getVal(), Toast.LENGTH_SHORT).show();
		}
		else if(dataSetIndex == 2){
			Toast.makeText(getActivity(), "Your Pulse Rate was "+e.getVal(), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onNothingSelected() {
		// TODO Auto-generated method stub
		
	}
	
	

}
