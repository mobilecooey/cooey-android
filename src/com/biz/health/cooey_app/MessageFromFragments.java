package com.biz.health.cooey_app;

public interface MessageFromFragments {

	public void OnMessage(FragmentToActivityPayload payload);
}
