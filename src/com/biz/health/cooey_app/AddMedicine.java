package com.biz.health.cooey_app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.biz.cooey.SendJsonToCooeyTask;
import com.biz.cooey.SendToCooeyCallbackOnResponse;
import com.biz.health.model.MedicineData;
import com.biz.health.servermodel.MedServerDataPut;
import com.biz.health.utils.db.CooeyMedDataSource;
import com.google.gson.GsonBuilder;


public class AddMedicine extends Fragment {
	
	private FetchDataFromUrl fetch = new FetchDataFromUrl();
	private List<MedicineModel> COUNTRIES;
	private MedicineAdapter medAdapter;
	//private ExpandableListAdapter listAdapter, listAdapterD;
    private ExpandableListView expListView, expListViewD;
    private ImageView addMedicine,  clearMedicine;
    private View rootView = null;
    private AlertDialog dosageDialog = null, reminderDialog = null;
    private InputMethodManager imm = null;
    private List<String> reminderdays = new ArrayList<String>();
    private List<String> dosages = new ArrayList<String>();
    private List<MedicineData> mLstRecentMeds = new ArrayList<MedicineData>();
    private MyRecentMedsAdapter<MedicineData> recmedsadapter = null;
    private ListView recentmedslv = null;
    private MedicineModel currentSelectedMedModel = null;
    private Typeface QS_font, QS_fontbook;
    private Button btnDosage, skipMedicine;
    private ScrollView svwMainMed;
    private TextView txtAftrB, txtBfrB, txtAftrL, txtBfrL, txtAftrD, txtBfrD, txtBTime, txtLTime, txtDTime; 
    private ChosenFoodTimes foodtimes;
    
    private class ChosenFoodTimes{
    	public String bb="0", ab="0", bl="0", al="0", bd="0", ad="0";
    	
    	public String getFoodTimesString(){
    		String freq = bb+"-"+ab+","+bl+"-"+al+","+bd+"-"+ad;
    		return freq;
    	}
    	
    	public void resetTimes(){
    		bb="0"; ab="0"; bl="0"; al="0"; bd="0"; ad="0";
    	}
    }
    
	public AddMedicine() {
		//onSuccess = _onSuccess;
		COUNTRIES = new ArrayList<MedicineModel>();
		MedicineModel hm = new MedicineModel();
		hm.setMedName("Crocin");
		hm.setMedComp("Paracetamol");
		dosages.add("0"); dosages.add("mg");
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		activity.getActionBar().setTitle("ADD MEDICATION");
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AutoCompleteTextView ac = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		if( !ac.getHint().equals("")){
			hideViews(rootView);
		}
		else if(!ac.getText().equals("")){
			showViews(rootView);
		}
		//((MainActivity)getActivity()).setActionBarText("ADD MEDICATION");
		getActivity().getActionBar().setTitle("ADD MEDICATION");
		clearForNewMedicine();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.medicine, menu);	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch(id){
			case R.id.mydashfrommed:
				getFragmentManager().popBackStack();
				getFragmentManager().beginTransaction().replace(R.id.container, 
						((MainActivity)getActivity()).getDashboard(), "").addToBackStack(null).commit();//.addToBackStack(null)
				return true;
			case R.id.listfrommed:
				MyMedicines mm = new MyMedicines();
				getFragmentManager().beginTransaction().replace(R.id.container, mm, "medList").addToBackStack(null).commit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	public class FetchDataFromUrl extends AsyncTask<String, Void, JSONObject>{
		InputStream is = null;
		String json = "";
		JSONObject jObj;
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			if(isCancelled()) return null;
			String param0 = params[0];
			 try {
			      // defaultHttpClient
			      DefaultHttpClient httpClient = new DefaultHttpClient();
			      HttpGet httpGet = new HttpGet(param0);
			      HttpResponse httpResponse = httpClient.execute(httpGet);
			      HttpEntity httpEntity = httpResponse.getEntity();
			      is = httpEntity.getContent();
			    } catch (UnsupportedEncodingException e) {
			      e.printStackTrace();
			    } catch (ClientProtocolException e) {
			      e.printStackTrace();
			    } catch (IOException e) {
			      e.printStackTrace();
			    }
			 try {
				  BufferedReader reader = new BufferedReader(new InputStreamReader(
				      is, "iso-8859-1"), 8);
				  StringBuilder sb = new StringBuilder();
				  String line = null;
				  while ((line = reader.readLine()) != null) {
				    sb.append(line + "n");
				  }
				  is.close();
				  json = sb.toString();
			    } catch (Exception e) {
			      Log.e("Buffer Error", "Error converting result " + e.toString());
			    }
			    // try parse the string to a JSON object
			    try {
			      jObj = new JSONObject(json);
			    } catch (JSONException e) {
			      Log.e("JSON Parser", "Error parsing data " + e.toString());
			    }
			    // return JSON String
			    return jObj;
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result == null){
				medAdapter.clear();
				return;
			}
			try {
				
				if(result.get("medication") == null){
					medAdapter.clear();
					return;
				}
				if(result.get("medication").toString() == "null"){
					medAdapter.clear();
					return;
				}
				JSONArray meds = result.getJSONArray("medication");
				medAdapter.clear();
				for (int i=0; i< meds.length(); ++i){
					Log.d("FOR EACH", (String) ((JSONObject) meds.get(i)).get("name"));
					MedicineModel m = new MedicineModel();
					m.setMedName((String) ((JSONObject) meds.get(i)).get("name"));
					if(((JSONObject) meds.get(i)).get("base_component")!=null)
					{
						JSONArray tmp = (JSONArray) ((JSONObject) meds.get(i)).get("base_component");
						m.setMedComp(tmp.toString());
					}
					m.setMedId((String) ((JSONObject) meds.get(i)).get("medicineId"));
					medAdapter.add(m);
					//medAdapter.add((String) ((JSONObject) meds.get(i)).get("name"));
				}
				Log.d("COUNT", ""+medAdapter.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//update UI
			//medAdapter.notifyDataSetChanged();
		}
	}

	private void searchMedicine(String name){
		if(name.startsWith("{") && name.endsWith("}")){
			name = name.replace("{", "").replace("}", "");
			name = name.split("=")[0];
		}
		//String srch = "http://asl002.cloudapp.net:9010/ehealth/v1/actvity/medicine/search/"+name;
		//http://api.cooey.co.in/ehealth/v1/actvity/medicine/search/test/5
		String srch = "http://api.cooey.co.in/ehealth/v1/actvity/medicine/search/"+name+"/0";
		if(!fetch.isCancelled()){ 
			fetch.cancel(true);
		
			fetch = new FetchDataFromUrl(); 
			fetch.execute(new String[] {srch});
		}
		
		return;
	}
	
	private String[] getWeekDaysInNums(){
		List<String> daysWeek = new ArrayList<String>(7);
	
		for (String string : reminderdays) {
			if(string.equalsIgnoreCase("mon")){ daysWeek.add("1");}
			if(string.equalsIgnoreCase("tue")){ daysWeek.add("2");}
			if(string.equalsIgnoreCase("wed")){ daysWeek.add("3");}
			if(string.equalsIgnoreCase("thu")){ daysWeek.add("4");}
			if(string.equalsIgnoreCase("fri")){ daysWeek.add("5");}
			if(string.equalsIgnoreCase("sat")){ daysWeek.add("6");}
			if(string.equalsIgnoreCase("sun")){ daysWeek.add("7");}			
		}
		String[] stockArr = new String[daysWeek.size()];	
		stockArr = daysWeek.toArray(stockArr);
		return stockArr;
	}
	
	private MedicineData getUserChosenMedicine(){
		MedicineData md = new MedicineData();
		AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		md.setMedicineName(currentSelectedMedModel.getMedName()) ;//actv.getText().toString());
		
		TextView tv = (TextView)rootView.findViewById(R.id.medComp);
		md.setBaseComponent(tv.getText().toString()); //TODO: use currentSelectedMedModel
		
		md.setMedicineId(currentSelectedMedModel.getMedId());		
		
		md.setFrequency(foodtimes.getFoodTimesString());
		
		if(reminderdays.size() != 0) md.setIsRemind(1);
		md.setReminders(TextUtils.join(",", reminderdays));
		
		md.setDosage_unit(dosages.get(1));
		md.setRemind("true");
		md.setDosage_times(new String[] {"time1", "time2"});		
		md.setActive(true);
		md.setDosage(dosages.get(0));//+" "+dosages.get(1));
	
		md.setDaysWeek(getWeekDaysInNums());
		md.setTod("1-0-0");
		
		return md;
	}
	
	private void addMedicineWrapper(long uid){
		
		MedicineData toadd = getUserChosenMedicine();
		CooeyMedDataSource ds = new CooeyMedDataSource(getActivity());
		ds.open();
		ds.createMedicineForuser(toadd, uid);
		ds.close();
		//send to server
		mLstRecentMeds.add(0, toadd);
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(getActivity(), "Medicine Added", Toast.LENGTH_SHORT).show();	
				//recmedsadapter.notifyDataSetChanged();
			}
		});
		MedServerDataPut msd = new MedServerDataPut();
		msd.md = toadd;
		msd.patientId = ((MainActivity)getActivity()).getActiveProfile().getPatientId();
		
		String mMedJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(msd);
		Log.d(" MEDICINE DATA TO POST ", mMedJson);
		SendJsonToCooeyTask sendMed = (SendJsonToCooeyTask) new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {
			
			@Override
			public String getUrl() {
				return "http://api.cooey.co.in/ehealth/v1/profile/medicine";
			}
			
			@Override
			public void callme(String resp, int code) {
				// TODO Auto-generated method stub
				int x = 0;
			}

			@Override
			public void callmeWithContext(String resp, Context context) {
				// TODO Auto-generated method stub
				
			}
		}).execute(mMedJson);
		clearForNewMedicine();
		hideViews(rootView);
	}
	
	public class AddMedicineListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			addMedicineWrapper(((MainActivity)getActivity()).getActiveProfile().getDbid());
			//Fragment f = ((MainActivity)getActivity()).getNextFlowAddMed();
			//getFragmentManager().beginTransaction().replace(R.id.container, f, "").addToBackStack(null).commit();
		}
	}
	
	public class AddMoreMedicineListener implements OnClickListener{

		@Override
		public void onClick(View v) {			
			clearForNewMedicine();
			hideViews(rootView);
		}
	}
	
	private void showRecentMeds(){
		
		LinearLayout rlyt  = (LinearLayout)rootView.findViewById(R.id.rlytrecentmeds);
		rlyt.setVisibility(View.VISIBLE);
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				recmedsadapter.notifyDataSetChanged();
				recentmedslv.post(new Runnable() {
					
					@Override
					public void run() {
						recentmedslv.smoothScrollToPosition(0);;
						
					}
				});
			}
		});
	}
	
	private void hideRecentMeds(){
		
		LinearLayout rlyt  = (LinearLayout)rootView.findViewById(R.id.rlytrecentmeds);
		rlyt.setVisibility(View.GONE);		
	}
	
	private void clearForNewMedicine(){
		//clear up all views and hide.
		AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		actv.setText("");
		actv.setHint(R.string.medicineName);
		TextView tv = (TextView)rootView.findViewById(R.id.medComp);
		tv.setText("");
		tv.setHint(R.string.med_base_comp);
		
		//reset the food times
		if(foodtimes == null)
			foodtimes = new ChosenFoodTimes();
		
		foodtimes.resetTimes();
	}
	
	public class SkipMedicineListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			Fragment f = ((MainActivity)getActivity()).getNextFlowAddMed();
			getFragmentManager().beginTransaction().replace(R.id.container, f, "").addToBackStack(null).commit();
		}
	}
	
	private void hideViews(View parent){
		
		ScrollView sv = (ScrollView) parent.findViewById(R.id.scrladdmedvw);
		sv.setVisibility(View.INVISIBLE);
		((Button)parent.findViewById(R.id.btnDosage)).setVisibility(View.INVISIBLE);
		RelativeLayout rlyt = (RelativeLayout)parent.findViewById(R.id.relytaddbtns);
		rlyt.setVisibility(View.INVISIBLE);
		showRecentMeds();
	}
	
	private void showViews(View parent){

		btnDosage.setVisibility(View.VISIBLE);		
		svwMainMed.setVisibility(View.VISIBLE);		
		RelativeLayout rlyt = (RelativeLayout)parent.findViewById(R.id.relytaddbtns);
		rlyt.setVisibility(View.VISIBLE);
		
		AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		imm.hideSoftInputFromWindow(actv.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		hideRecentMeds();
	}
	
	private void prepareDialogs(){
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View reminder = inflater.inflate(R.layout.reminder_items, null);
		final LinearLayout dos = (LinearLayout) inflater.inflate(R.layout.dosage_items, null);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(reminder)
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   reminderdays.clear();
                       if(((ToggleButton)reminder.findViewById(R.id.tglMon)).isChecked()){
                    	   reminderdays.add("Mon");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglTue)).isChecked()){
                    	   reminderdays.add("Tue");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglWed)).isChecked()){
                    	   reminderdays.add("Wed");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglThu)).isChecked()){
                    	   reminderdays.add("Thu");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglFri)).isChecked()){
                    	   reminderdays.add("Fri");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglSat)).isChecked()){
                    	   reminderdays.add("Sat");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglSun)).isChecked()){
                    	   reminderdays.add("Sun");
                       }
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // User cancelled the dialog
                   }
               });
        // Create the AlertDialog object and return it
        reminderDialog = builder.create();
        
        
        ArrayAdapter<String> ba = new ArrayAdapter<String>(getActivity().getApplicationContext(), 
        		R.layout.spinner_cooey_dropdown_item, 
        		getResources().getStringArray(R.array.dosage));
        
        ((Spinner)dos.findViewById(R.id.medSpinDosage)).setAdapter(ba);
        AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
        dosageDialog = 
        		builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   dosages.add(0, ((TextView)dos.findViewById(R.id.medTxtDosage)).getText().toString());
                	   dosages.add(1, (String) ((Spinner)dos.findViewById(R.id.medSpinDosage)).getSelectedItem());
                	   
                	   imm.hideSoftInputFromWindow(dos.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   imm.hideSoftInputFromWindow(dos.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                   }
               }).setView(dos)
               //.setSingleChoiceItems(new String[]{"mg", "g", "capsule/s", "bottle/s"}, 0, null)               
               .create();

	}
	
	private void populateExpandableListViews(View parent){
		
		List<String> listDataHeader = new ArrayList<String>();
        listDataHeader.add("        Remind?");
        List<String> top250 = new ArrayList<String>();
        top250.add(" ");
        HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
        
        listDataChild.put(listDataHeader.get(0), top250);
        
		//expListView = (ExpandableListView)parent.findViewById(R.id.lvExp);
        //listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        /// setting list adapter
        //expListView.setAdapter(listAdapter);
        
        /*
        List<String> listDataHeaderD = new ArrayList<String>();
        listDataHeaderD.add("        Dosage");
        List<String> ch = new ArrayList<String>();
        ch.add("d");
        HashMap<String, List<String>> listDataChildD = new HashMap<String, List<String>>();
        
        listDataChildD.put(listDataHeaderD.get(0), ch);
        
		expListViewD = (ExpandableListView)parent.findViewById(R.id.lvExpDos);
        listAdapterD = new ExpandableListAdapter(getActivity(), listDataHeaderD, listDataChildD);
        expListViewD.setAdapter(listAdapterD);*/
        
	}
	
	private void populateRecentMeds(){
		
		CooeyMedDataSource ds = new CooeyMedDataSource(getActivity());
		ds.open();
		mLstRecentMeds = ds.getMedicinesForUser(((MainActivity)getActivity()).getActiveProfile().getDbid(), " date DESC ");
		ds.close();		
	}
	
	private void setupNavButtons(){
		
		if( ((MainActivity)getActivity()).isUserOnboarding() )
		{
			skipMedicine = (Button)rootView.findViewById(R.id.skipMedBtn);
			skipMedicine.setOnClickListener(new SkipMedicineListener());
		}else{
			skipMedicine = (Button)rootView.findViewById(R.id.skipMedBtn);
			skipMedicine.setText("Back");
			skipMedicine.setBackgroundResource(R.drawable.lt2brdvwdark2);
			skipMedicine.setTextColor( getActivity().getResources().getColor(R.color.applt3));
			skipMedicine.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getFragmentManager().beginTransaction().replace(R.id.container, 
							((MainActivity)getActivity()).getDashboard(), "")
							.addToBackStack(null)
							.commit();
				}
			});
		}
		
	}
	
	private void setupFonts(){
		btnDosage.setTypeface(QS_font);
		txtAftrB.setTypeface(QS_font);
		txtBTime.setTypeface(QS_font);
		txtBfrB.setTypeface(QS_font);
		
		txtLTime.setTypeface(QS_font);
		txtAftrL.setTypeface(QS_font);
		txtBfrL.setTypeface(QS_font);
		
		txtDTime.setTypeface(QS_font);
		txtBfrD.setTypeface(QS_font);
		txtAftrD.setTypeface(QS_font);
		((TextView)rootView.findViewById(R.id.txtBfast)).setTypeface(QS_fontbook);
		((TextView)rootView.findViewById(R.id.txtLunch)).setTypeface(QS_fontbook);
		((TextView)rootView.findViewById(R.id.txtDinner)).setTypeface(QS_fontbook);
		((TextView)rootView.findViewById(R.id.medComp)).setTypeface(QS_font);
	}
	
	
	public class GetTimeFromDialog implements View.OnClickListener{

		private void setTextAfterTime(final View v, int hourOfDay, int minute){
			
			int hr = 0;
        	String am_pm = "AM";
        	String min = String.valueOf(minute);
        	if(hourOfDay >= 12){
        		hr = hourOfDay % 12;
        		if(hr == 0){ hr = 12; };
        		am_pm = "PM";
        	}else{
        		hr = hourOfDay;
        	}
        	if(minute < 10){
        		min = "0"+min;//some plumbing :(
        	}
        	
			((TextView)v).setText(String.valueOf(hr) + ":" + min + " "+am_pm);
            ((TextView)v).setTextSize(20f);
            ((TextView)v).setTextColor(getActivity().getResources().getColor(R.color.greenreading));
            ((TextView)v).setBackgroundResource(R.drawable.lt2brdvwlt2);
		}
		
		@Override
		public void onClick(final View v) {
			final Calendar c = Calendar.getInstance();
			final int mHour = c.get(Calendar.HOUR_OF_DAY);
            final int mMinute = c.get(Calendar.MINUTE);
            TimePickerDialog tpd = null;
			switch(v.getId())
			{
				case R.id.txtDnrSettime:
		            tpd = new TimePickerDialog(getActivity(),
		            		new TimePickerDialog.OnTimeSetListener() {
		       		 
		                        @Override
		                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		                        	
		                        	setTextAfterTime(v, hourOfDay, minute);
		                        	//set dinner time
		                        }
		                    }, mHour, mMinute, false);
		            tpd.show();
					break;
				case R.id.txtLnchSettime:
					tpd = new TimePickerDialog(getActivity(),
		            		new TimePickerDialog.OnTimeSetListener() {
		       		 
		                        @Override
		                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		                        	
		                        	setTextAfterTime(v, hourOfDay, minute);
		                        	//set Lunch time
		                        }
		                    }, mHour, mMinute, false);
		            tpd.show();
					break;
				case R.id.txtBfastSettime:
					tpd = new TimePickerDialog(getActivity(),
		            		new TimePickerDialog.OnTimeSetListener() {
		       		 
		                        @Override
		                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		                        	
		                        	setTextAfterTime(v, hourOfDay, minute);
		                        	//set bfast time
		                        }
		                    }, mHour, mMinute, false);
		            tpd.show();
					break;					
			}			
		}
		
	}
	
	public class ToggleFoodText implements View.OnClickListener{

		private void setTextColor(View v, int color, float size){
			((TextView)v).setTextColor(getActivity().getResources().getColor(color));
			((TextView)v).setTextSize(size);
		}
		@Override
		public void onClick(final View v) {
			int clrlt4 = R.color.applt4;
			int clrgrn = R.color.greenreading;
			switch(v.getId())
			{
				
				case R.id.txttglAftrBfast:
					if(foodtimes.ab.equalsIgnoreCase("1")){
						foodtimes.ab = "0";
						setTextColor(v, clrlt4, 18f);
						break;
					}
					foodtimes.ab = "1";					
					setTextColor(v, clrgrn, 20f);
					break;
				case R.id.txttglBfrBfast:
					if(foodtimes.bb.equalsIgnoreCase("1")){
						foodtimes.bb = "0";
						setTextColor(v, clrlt4, 18f);
						break;
					}
					foodtimes.bb = "1";					
					setTextColor(v, clrgrn, 20f);
					break;
				case R.id.txttglAftrLnch:
					if(foodtimes.al.equalsIgnoreCase("1")){
						foodtimes.al = "0";
						setTextColor(v, clrlt4, 18f);
						break;
					}
					foodtimes.al = "1";					
					setTextColor(v, clrgrn, 20f);
					break;
				case R.id.txttglBfrLnch:
					if(foodtimes.bl.equalsIgnoreCase("1")){
						foodtimes.bl = "0";
						setTextColor(v, clrlt4, 18f);
						break;
					}
					foodtimes.bl = "1";					
					setTextColor(v, clrgrn, 20f);
					break;
				case R.id.txttglBfrDnr:
					if(foodtimes.bd.equalsIgnoreCase("1")){
						foodtimes.bd = "0";
						setTextColor(v, clrlt4, 18f);
						break;
					}
					foodtimes.bd = "1";					
					setTextColor(v, clrgrn, 20f);
					break;
				case R.id.txttglAftrDnr:
					if(foodtimes.ad.equalsIgnoreCase("1")){
						foodtimes.ad = "0";
						setTextColor(v, clrlt4, 18f);
						break;
					}
					foodtimes.ad = "1";					
					setTextColor(v, clrgrn, 20f);
					break;			
			}				
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		rootView = inflater.inflate(R.layout.addmedicine, container, false);
		
		final TextView tv = (TextView)rootView.findViewById(R.id.medComp);
		final AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		imm = (InputMethodManager)getActivity().getSystemService (Context.INPUT_METHOD_SERVICE);
		recentmedslv = (ListView)rootView.findViewById(R.id.lstrecentmeds);
		QS_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Light.otf");
		QS_fontbook =Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");
		GetTimeFromDialog getTime = new GetTimeFromDialog();
		ToggleFoodText foodText = new ToggleFoodText();
		
		
		btnDosage = (Button)rootView.findViewById(R.id.btnDosage);
		svwMainMed = (ScrollView)rootView.findViewById(R.id.scrladdmedvw);
		
		txtAftrB = (TextView)rootView.findViewById(R.id.txttglAftrBfast);
		txtBTime = (TextView)rootView.findViewById(R.id.txtBfastSettime);
		txtBfrB = (TextView)rootView.findViewById(R.id.txttglBfrBfast); //, txtLAftr, txtLBfr, txtDAftr, txtDBfr;
		
		txtLTime = (TextView)rootView.findViewById(R.id.txtLnchSettime);
		txtAftrL =(TextView)rootView.findViewById(R.id.txttglAftrLnch);
		txtBfrL =(TextView)rootView.findViewById(R.id.txttglBfrLnch);
		
		txtDTime = (TextView)rootView.findViewById(R.id.txtDnrSettime);
		txtBfrD = (TextView)rootView.findViewById(R.id.txttglBfrDnr);
		txtAftrD = (TextView)rootView.findViewById(R.id.txttglAftrDnr);
		
		clearMedicine = (ImageView)rootView.findViewById(R.id.btnclearmed);
		addMedicine = (ImageView)rootView.findViewById(R.id.addMedBtn);
		recentmedslv = (ListView)rootView.findViewById(R.id.lstrecentmeds);
		
		txtBTime.setOnClickListener(getTime);
		txtDTime.setOnClickListener(getTime);
		txtLTime.setOnClickListener(getTime);
		
		txtAftrB.setOnClickListener(foodText);
		txtBfrB.setOnClickListener(foodText);
		txtAftrD.setOnClickListener(foodText);
		txtBfrD.setOnClickListener(foodText);
		txtAftrL.setOnClickListener(foodText);
		txtBfrL.setOnClickListener(foodText);

		clearMedicine.setOnClickListener(new AddMoreMedicineListener());
		addMedicine.setOnClickListener(new AddMedicineListener());
		
		setupFonts();
		setupNavButtons();
		populateRecentMeds();       
		setHasOptionsMenu(true);
		prepareDialogs();
		
		ImageView reminder = (ImageView)rootView.findViewById(R.id.addRemindBtn);
		reminder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				reminderDialog.show();				
			}
		});
		
		       
        recmedsadapter = new MyRecentMedsAdapter<MedicineData>(getActivity().getApplicationContext(), 
        					R.layout.my_recentmeds, mLstRecentMeds);
        recentmedslv.setAdapter(recmedsadapter);
        
		medAdapter = new MedicineAdapter((Context)getActivity(), android.R.layout.simple_dropdown_item_1line, COUNTRIES);
		
		actv.setAdapter(medAdapter);
		actv.setTypeface(QS_fontbook);
		actv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				currentSelectedMedModel= medAdapter.getItem(arg2);
				tv.setText(currentSelectedMedModel.getMedComp());
				actv.setText(currentSelectedMedModel.getMedName());// ((TextView)arg1.findViewById(android.R.id.text1)).getText());
				//TODO: populate dosages available as well.		
				
				showViews(rootView);
			}
		});
		actv.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				Log.d(MainActivity.class.getName(), s.toString());
				
				if(s.length() < 3){ medAdapter.clear(); return; }
				searchMedicine(s.toString().replace(" ", "%20"));
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});		
		return rootView;
	}	
}
