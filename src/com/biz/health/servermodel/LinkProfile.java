package com.biz.health.servermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LinkProfile {


	@Expose public long baseRelationId;
	@Expose public long toRelationId;
	@Expose @SerializedName("TID") public String tid;
	@Expose public String trackingId;
}
