package com.biz.health.servermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateProfile {

	@Expose @SerializedName("_id") private int id = 0;
	@Expose @SerializedName("DOB") private String dob = "1970-01-01";
	@Expose private String firstName = "";
	@Expose private String lastName = "";
	@Expose private String mobileNumber = "";
	@Expose private String externalID;
	@Expose private String gender= "";
	@Expose private String email = "";
	@Expose @SerializedName("country") private String locationCountry = "";
	@Expose private String profilePic = "";
	@Expose private String bloodGroup = "";
	@Expose private float weight = 0;
	@Expose private float height = 0;
	@Expose @SerializedName("city") private String locationCity= "";
	
	public String getBloodGroup() {
		return bloodGroup;
	}
	public String getDob() {
		return dob;
	}
	public String getEmail() {
		return email;
	}
	public String getExternalID() {
		return externalID;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getGender() {
		return gender;
	}
	public float getHeight() {
		return height;
	}
	public int getId() {
		return id;
	}
	public String getLastName() {
		return lastName;
	}
	public String getLocationCity() {
		return locationCity;
	}
	public String getLocationCountry() {
		return locationCountry;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public float getWeight() {
		return weight;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setExternalID(String externalID) {
		this.externalID = externalID;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
}
