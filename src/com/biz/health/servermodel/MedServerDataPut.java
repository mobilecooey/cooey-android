package com.biz.health.servermodel;

import com.biz.health.model.MedicineData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedServerDataPut {

	@Expose @SerializedName("medicine") public MedicineData md;
	@Expose public int patientId;
}
