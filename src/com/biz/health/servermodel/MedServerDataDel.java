package com.biz.health.servermodel;

import com.google.gson.annotations.Expose;

public class MedServerDataDel {

	@Expose public String medicineId;
	@Expose public int profileId;
	
}
