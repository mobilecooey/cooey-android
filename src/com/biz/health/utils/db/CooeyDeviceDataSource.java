package com.biz.health.utils.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.biz.cooey.CooeyCode;
import com.biz.cooey.CooeyStatus;
import com.lifesense.ble.bean.LsDeviceInfo;

public class CooeyDeviceDataSource {

	private SQLiteDatabase database;
	private CooeySQLHelper dbHelper;

	private String[] allColumns = { CooeySQLHelper.COL_ID,
			CooeySQLHelper.USR_ID, 
			CooeySQLHelper.KEY_DEVICE_ID,
			CooeySQLHelper.KEY_DEVICE_NAME, 
			CooeySQLHelper.KEY_DEVICE_ADDRESS,
			CooeySQLHelper.KEY_DEVICE_SERVICE_UUID,
			CooeySQLHelper.KEY_DEVICE_TYPE,
			CooeySQLHelper.KEY_DEVICE_PAIRFLAGS, 
			CooeySQLHelper.KEY_DEVICE_SN,
			CooeySQLHelper.KEY_DEVICE_MODELNUMBER,
			CooeySQLHelper.KEY_DEVICE_PASSWORD,
			CooeySQLHelper.KEY_DEVICE_BROADCASTID,
			CooeySQLHelper.KEY_DEVICE_SOFTWARE_VERSION,
			CooeySQLHelper.KEY_DEVICE_HARDWARE_VERSION,
			CooeySQLHelper.KEY_DEVICE_FIRMWARE_VERSION,
			CooeySQLHelper.KEY_DEVICE_MANUFACTURENAME,
			CooeySQLHelper.KEY_DEVICE_SYSTEMID,
			CooeySQLHelper.KEY_DEVICE_MODEL,
			CooeySQLHelper.KEY_DEVICE_USER_NUMBER,
			CooeySQLHelper.KEY_DEVICE_USER_NAME,
			CooeySQLHelper.KEY_MAX_USER_QUANTITY,
			CooeySQLHelper.KEY_DEVICE_STATUS, 
			CooeySQLHelper.KEY_PROTOCOL_TYPE,
			CooeySQLHelper.KEY_PAIRSTATUS};

	public CooeyDeviceDataSource(Context context) {
		dbHelper = new CooeySQLHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createDeviceForUser(LsDeviceInfo dev, long uid) {
		//First query for exisiting device, re-pair already paired device.
		Cursor cursor = null;
		if(dev.getProtocolType().contains("A3")){
			cursor = database.query(CooeySQLHelper.TABLE_DEV, allColumns,
					CooeySQLHelper.USR_ID + "=" + uid
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_USER_NUMBER + " = "+ dev.getDeviceUserNumber()
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_ID + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_SN + " = ?",
					new String[] { dev.getDeviceId(), dev.getDeviceSn()},
					null, null, null, null);
		}else if(dev.getProtocolType().contains("A2")){			
			cursor = database.query(CooeySQLHelper.TABLE_DEV, allColumns,
					CooeySQLHelper.USR_ID + "=" + uid
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_ID + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_SN + " = ?",
					new String[] { dev.getDeviceId(), dev.getDeviceSn()},
					null, null, null, null);
		}
		
		if(cursor != null)
		{			
			if(cursor.moveToFirst()) {
				//delete it and then add the new one
				String rowId = cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_ID));
				database.delete(CooeySQLHelper.TABLE_DEV, CooeySQLHelper.COL_ID + "=?",  new String[]{rowId});
			}
		}
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.USR_ID, uid);
		values.put(CooeySQLHelper.KEY_DEVICE_ID, dev.getDeviceId());
		values.put(CooeySQLHelper.KEY_DEVICE_NAME, dev.getDeviceName());
		values.put(CooeySQLHelper.KEY_DEVICE_TYPE, dev.getDeviceType());
		values.put(CooeySQLHelper.KEY_DEVICE_SN, dev.getDeviceSn());
		values.put(CooeySQLHelper.KEY_DEVICE_MODELNUMBER, dev.getModelNumber());
		values.put(CooeySQLHelper.KEY_DEVICE_PASSWORD, dev.getPassword());
		values.put(CooeySQLHelper.KEY_DEVICE_BROADCASTID, dev.getBroadcastID());
		values.put(CooeySQLHelper.KEY_DEVICE_SOFTWARE_VERSION,
				dev.getSoftwareVersion());
		values.put(CooeySQLHelper.KEY_DEVICE_HARDWARE_VERSION,
				dev.getHardwareVersion());
		values.put(CooeySQLHelper.KEY_DEVICE_FIRMWARE_VERSION,
				dev.getFirmwareVersion());
		values.put(CooeySQLHelper.KEY_DEVICE_MANUFACTURENAME,
				dev.getManufactureName());
		values.put(CooeySQLHelper.KEY_DEVICE_SYSTEMID, dev.getSystemId());
		values.put(CooeySQLHelper.KEY_DEVICE_MODEL, "");
		values.put(CooeySQLHelper.KEY_DEVICE_USER_NUMBER,
				dev.getDeviceUserNumber());
		values.put(CooeySQLHelper.KEY_DEVICE_USER_NAME,
				dev.getDeviceUserNumber());
		values.put(CooeySQLHelper.KEY_MAX_USER_QUANTITY,
				dev.getMaxUserQuantity());
		values.put(CooeySQLHelper.KEY_DEVICE_STATUS, "");
		values.put(CooeySQLHelper.KEY_DEVICE_SERVICE_UUID, "");
		values.put(CooeySQLHelper.KEY_DEVICE_PAIRFLAGS, "");
		values.put(CooeySQLHelper.KEY_PROTOCOL_TYPE, dev.getProtocolType());
		values.put(CooeySQLHelper.KEY_PAIRSTATUS, dev.getPairStatus());

		long insertId = database.insert(CooeySQLHelper.TABLE_DEV, null, values);

		cursor = database.query(CooeySQLHelper.TABLE_DEV, allColumns,
				CooeySQLHelper.COL_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		// CooeyProfile newComment = cursorToComment(cursor);

		cursor.close();
	}

	public CooeyStatus getRemoveDeviceForUser(LsDeviceInfo dev, long uid){
		int deleteId = -1;
		//deviceId=61AC09819AF2, deviceSn=0640103308493810/
		if(dev.getProtocolType().contains("A3")){
				deleteId = database.delete(CooeySQLHelper.TABLE_DEV, 
					CooeySQLHelper.USR_ID + "=" + uid
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_USER_NUMBER + " = "+ dev.getDeviceUserNumber()
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_PASSWORD + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_ID + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_SN + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_BROADCASTID + " = ?",
					new String[] { dev.getPassword(), dev.getDeviceId(), dev.getDeviceSn(), dev.getBroadcastID()});
			
		}else if(dev.getProtocolType().contains("A2")){
				deleteId = database.delete(CooeySQLHelper.TABLE_DEV, 
					CooeySQLHelper.USR_ID + "=" + uid
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_ID + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_SN + " = ?"
					+ " and "
					+ CooeySQLHelper.KEY_DEVICE_BROADCASTID + " = ?",
					new String[] { dev.getDeviceId(), dev.getDeviceSn(), dev.getBroadcastID()});
		}
		if(deleteId >= 0){
			return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("Content Deleted");
		}
		if(deleteId == 0){return new CooeyStatus().setStatus(CooeyCode.COOEY_NOTHING).setMessage("Nothing to delete");}
		return new CooeyStatus().setStatus(CooeyCode.COOEY_FAIL).setMessage("Delete Failure");
	}
	
	public List<LsDeviceInfo> getDevicesForUser(long uid) {
		List<LsDeviceInfo> devList = new ArrayList<LsDeviceInfo>();

		Cursor cursor = database.query(CooeySQLHelper.TABLE_DEV, allColumns,
				CooeySQLHelper.USR_ID + " = " + uid, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			LsDeviceInfo prof = cursorToDevice(cursor);
			devList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return devList;
	}
	
	public List<LsDeviceInfo> getAllDevices() {
		List<LsDeviceInfo> devList = new ArrayList<LsDeviceInfo>();

		Cursor cursor = database.query(CooeySQLHelper.TABLE_DEV, allColumns,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			LsDeviceInfo prof = cursorToDevice(cursor);
			devList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return devList;
	}

	public LsDeviceInfo cursorToDevice(Cursor cursor) {
		LsDeviceInfo dev = new LsDeviceInfo();
	
		dev.setBroadcastID(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_BROADCASTID)));
		dev.setDeviceId(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_ID)));
		dev.setDeviceName(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_NAME)));
		dev.setDeviceSn(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_SN)));
		dev.setDeviceType(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_TYPE)));
		dev.setDeviceUserNumber(cursor.getInt(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_USER_NUMBER)));
		dev.setFirmwareVersion(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_FIRMWARE_VERSION)));
		dev.setHardwareVersion(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_HARDWARE_VERSION)));
		dev.setManufactureName(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_MANUFACTURENAME)));
		dev.setMaxUserQuantity(cursor.getInt(cursor.getColumnIndex(CooeySQLHelper.KEY_MAX_USER_QUANTITY)));
		dev.setModelNumber(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_MODELNUMBER)));
		dev.setPairStatus(cursor.getInt(cursor.getColumnIndex(CooeySQLHelper.KEY_PAIRSTATUS)));
		dev.setPassword(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_PASSWORD)));
		dev.setProtocolType(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_PROTOCOL_TYPE)));
		dev.setSoftwareVersion(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_SOFTWARE_VERSION)));
		dev.setSystemId(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.KEY_DEVICE_SYSTEMID)));
		//TODO TODO TODO, dirty hack, WARNING
		dev.setMaxUserQuantity(cursor.getInt(cursor.getColumnIndex(CooeySQLHelper.USR_ID)));
		return dev;
		
	}
}
