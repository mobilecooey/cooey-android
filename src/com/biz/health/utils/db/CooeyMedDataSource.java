package com.biz.health.utils.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.biz.cooey.CooeyCode;
import com.biz.cooey.CooeyStatus;
import com.biz.cooey.WeightData;
import com.biz.health.model.MedicineData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CooeyMedDataSource {

	private SQLiteDatabase database;
	private CooeySQLHelper dbHelper;

	private String[] allColumns = { CooeySQLHelper.COL_ID,    
			CooeySQLHelper.COL_MEDNAME, 
			CooeySQLHelper.COL_BCOMP,
			CooeySQLHelper.COL_DOSAGE, 
			CooeySQLHelper.COL_ISREMIND,
			CooeySQLHelper.COL_REMINDER,
			CooeySQLHelper.COL_FREQ,
			CooeySQLHelper.COL_MEDTYPE,
			CooeySQLHelper.COL_DATE, CooeySQLHelper.COL_MEDID, 
			CooeySQLHelper.COL_DOSAGE_UNIT };
	    
	public CooeyMedDataSource(Context context) {
		dbHelper = new CooeySQLHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	public void createMedicineForuser(MedicineData md, long uid){
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.USR_ID, uid);
		values.put(CooeySQLHelper.COL_MEDNAME, md.getMedicineName());
		values.put(CooeySQLHelper.COL_BCOMP, md.getBaseComponent());
		values.put(CooeySQLHelper.COL_DOSAGE, md.getDosage());
		values.put(CooeySQLHelper.COL_REMINDER, md.getReminders());
		values.put(CooeySQLHelper.COL_ISREMIND, md.getIsRemind());
		values.put(CooeySQLHelper.COL_FREQ, md.getFrequency());
		values.put(CooeySQLHelper.COL_MEDTYPE, md.getMedicineType());
		values.put(CooeySQLHelper.COL_MEDID, md.getMedicineId());
		
		SimpleDateFormat DbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		values.put(CooeySQLHelper.COL_DATE,DbDateFormat.format(new Date()));

		long insertId = database.insert(CooeySQLHelper.TABLE_MED, null, values);
		if(insertId == -1){
			//something wrong
		}
		Cursor cursor = database.query(CooeySQLHelper.TABLE_MED, allColumns,
				CooeySQLHelper.COL_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		cursor.close();		
	}
	
	public List<MedicineData> getMedicinesForUser(long uid, String orderby){
		
		List<MedicineData> medList = new ArrayList<MedicineData>();
		
		Cursor cursor = database.query(CooeySQLHelper.TABLE_MED, allColumns,
				CooeySQLHelper.USR_ID + "=" + uid, 
				null, null, null, orderby);
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			MedicineData prof = cursorToMedicine(cursor);
			medList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return medList;		
	}
	
	public CooeyStatus removeMedicineForUser(MedicineData med, long uid){
		
		int deleteId = database.delete(CooeySQLHelper.TABLE_MED, 
				CooeySQLHelper.USR_ID + "=" + uid
				+ " and "
				+ CooeySQLHelper.COL_MEDNAME + " = \""+ med.getMedicineName() + "\"",
				new String[] {});
		
		if(deleteId >= 0){
			return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("Content Deleted");
		}
		if(deleteId == 0){return new CooeyStatus().setStatus(CooeyCode.COOEY_NOTHING).setMessage("Nothing to delete");}
		return new CooeyStatus().setStatus(CooeyCode.COOEY_FAIL).setMessage("Delete Failure");
	}
	
	public MedicineData cursorToMedicine(Cursor cursor) {
		MedicineData med = new MedicineData();
	
		med.setBaseComponent(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_BCOMP)));
		med.setMedicineName(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_MEDNAME)));
		med.setMedicineType(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_MEDTYPE)));
		med.setReminders(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_REMINDER)));
		med.setIsRemind(cursor.getInt(cursor.getColumnIndex(CooeySQLHelper.COL_ISREMIND)));
		med.setDosage(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_DOSAGE)));
		med.setFrequency(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_FREQ)));
		med.setDbId(cursor.getInt(cursor.getColumnIndex(CooeySQLHelper.COL_ID)));
		med.setDate(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_DATE)));
		med.setMedicineId(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_MEDID)));
		return med;		
	}

}
